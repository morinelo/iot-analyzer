CONF_FILES = ${shell ls runs/*.py}
LIST_NAME = $(CONF_FILES:runs/%.py=%)
LT_TARGETS = ltclockDriftNoPDR ltdiffHW ltnoClockDriftNoPDR ltnoClockDriftPDR ltforTable
BC_TARGETS = bcclockDriftNoPDR bcdiffHW bcnoClockDriftNoPDR bcnoClockDriftPDR
NRG_TARGETS = nrgEH

all: $(LT_TARGETS) $(BC_TARGETS) $(NRG_TARGETS)
	@echo "Congrats ! All pdf file have been generated."

paper : $(LT_TARGETS) $(NRG_TARGETS)
	@echo "Well done, all paper curves have been generated !"
		
lt% : runs/%.py
	@rm -f conf.pyc
	@echo "****** Lifetime curves : $* *******"
	@read -t 2 ||:;
	@cp -p runs/$*.py conf.py ;
	@make -f Makefile.run all MODE=lifetime;
	@echo "****** $* done *******"
	
nrg% : runs/%.py
	@rm -f conf.pyc
	@echo "****** Energy curves : $* *******"
	@read -t 2 ||:;
	@cp -p runs/$*.py conf.py ;
	@make -f Makefile.run all MODE=energyVaryingSaTa;
	@echo "****** $* done *******"

bc% : runs/%.py
	@rm -f conf.pyc
	@echo "****** Bit cost curves : $* *******"
	@read -t 2 ||:;
	@cp -p runs/$*.py conf.py ;
	@make -f Makefile.run all MODE=avgBitCost;
	@echo "****** $* done *******"
		
help :
	@echo "Use : make <mode><conf> (no space in-between)"
	@echo "\t*--------*"
	@echo "\t- Available conf : " 
	@echo "\t"$(LIST_NAME)
	@echo "\t- Available mode : "
	@echo "\tlt (lifetime), nrg (avg energy consumption) or bc (avg bit cost)"
	@echo "\t*--------*"
% :
	make -f Makefile.run $*
