from Techno import Techno
from fuzzyFloor import fuzzyFloor
from consoModeTimings import consoModeTimings
import conf
import globConf

class SIGFOX(Techno):
	
	def __init__(self, dc, hwname, version, drift = 'inf') :
		#No IPv6 for SIGFOX
	
		if (version == "100bps") : 
			rate = 0.1 * pow(10,-3) #Mbps, 100 bps
		elif (version == "1000bps") : 
			rate = 1 * pow(10,-3) #Mbps, 1000 bps
		else : 
			raise UserWarning("SIGFOX "+ str(version) + " is not implemented")
		
		self.version = version			

		#MAC ovhd smallest value = 60 bits 
		#Usual value = 112 bits total Ovhd
		#= 96 bits classiques + 16 bits de HMAC
		Techno.__init__(self, name = "SIGFOX"+str(version)+str(dc), \
							maxPacketSize = 12, \
							PHYRate = rate, \
							PHYOvhd = 36/rate, \
							MACOvhd = 60, \
							SixOvhd = 0,\
							clockDrift = drift,\
							platform_name = hwname)
		self.TiP = 0 # (us), timer inter data paquet (one data tx)
		self.TiTx = 0 # (us), time inter 3 Tx 
		self.DC = float(dc)

	def ackTime(self):	
		raise UserWarning("SIGFOX do no use ACK packets")
			
	def getAvailableHWList(self):
		from SIGFOXHardware import SIGFOXHardware
		return SIGFOXHardware.getList()	
	
	def ackedTransmissionIdleTime(self):
		return  0 # SIGFOX do no use ACK packets
		
	def ackedTransmissionRxTime(self):
		return  0 # SIGFOX do no use ACK packets
		
	def capacityOutputs(self, time):
		maxETSITime = (min(self.DC,1.0) / 100) * time	#maxETSI = 1%
		#How many full packet can we send
		fullTransmissionTime = (3*self.maxPacketTime()) \
								+ (2*self.TiTx) + self.TiP

		nbFullPacket = fuzzyFloor(\
							(maxETSITime*pow(10,3)) / fullTransmissionTime)
		nbTx = nbFullPacket

		if (globConf.SIGFOXDebug == True) : 
			print "<--- Capacity computation --- "
			print "Maximum Tx time allowed by ETSI : " + str(maxETSITime)+ " ms"
			print "Nb full packets = " + str(nbFullPacket)
		#Do we still have time to send some non-full packet ?
		#time leftover : removing non-useful time lost in overhead :
		deltaData =  (maxETSITime*pow(10,3)) % fullTransmissionTime \
					- 3 * self.totalOvhd() \
					- 2 * self.TiTx \
					- self.TiP
					
		if (deltaData > 0) :
			#Converting the time we have left into data size : 
			leftoverSize = fuzzyFloor((deltaData * self.PHYRate) / 3) #bits
			nbTx += 1
		else : 
			#Not enough time
			leftoverSize =  0 
	
		if (globConf.SIGFOXDebug == True) : 
			print "LeftoverSize = " + str(leftoverSize) + " bits"
		
		totalData = nbFullPacket * self.maxPacketSize * 8 + leftoverSize # bits	
		totalTime = time # ms 
		capaMax = totalData / totalTime
		txTime = (nbFullPacket * self.maxPacketTime() \
					+ self.dataPacketTime(leftoverSize) \
					)* pow(10,-3)
		rxTime = nbTx * self.ackedTransmissionRxTime() * pow(10,-3) 
		idleTime = nbTx * self.ackedTransmissionIdleTime() * pow(10,-3)
	
		outputs = { 	'totalData' : totalData, \
						'totalTime' : totalTime, \
						'capacityMax' : capaMax, \
						'txTime' : txTime, \
						'rxTime' : rxTime, \
						'idleTime' : idleTime } 
		return outputs	
			
	def energyConsumption(self, amountOfData, time, dc = 1):
		# This function assume amountOfData over time respect capacity constraints 
		# Return : nb of mJ used to transmit amountOfData (bits) within time (ms)
		nbFullPacketsNeeded = fuzzyFloor(float(amountOfData / 8) / self.maxPacketSize) #fuzzyFloor ok, % after
		lastPacketSize = float(amountOfData / 8) % self.maxPacketSize

		if (globConf.SIGFOXDebug == True) :	
			print "<----- Energy computation -----"
			myStr = "To send " + str(amountOfData) + "bits : " 
			myStr += str(nbFullPacketsNeeded) + " full packets + "
			myStr += str(lastPacketSize) + " bytes"
			print myStr

		# --- Full data packet transmissions
		txTime = self.maxPacketTime() * nbFullPacketsNeeded * 3 
		idleTime = self.ackedTransmissionIdleTime() * nbFullPacketsNeeded 
		rxTime  = self.ackedTransmissionRxTime() * nbFullPacketsNeeded
	
		if (globConf.SIGFOXDebug == True) :	
			print "Full Pckts :: "
			print "TDataMax " + str(txTime) + " ms"

		# --- Last packet transmissions
		if lastPacketSize != 0 : 
			txTime += self.dataPacketTime(lastPacketSize * 8) * 3
			idleTime += self.ackedTransmissionIdleTime()
			rxTime += self.ackedTransmissionRxTime()
		else : 
			pass #No need of an non full data packet

		#Convert us to s 
		outputs = {}
		outputs['timeTxMode'] = txTime * pow(10,-6)
		outputs['timeRxMode'] = rxTime * pow(10,-6)
		outputs['timeIdleMode'] = idleTime * pow(10,-6)
				
		#All timings are now in s
		globalTimings = consoModeTimings(	tx = outputs['timeTxMode'],\
											rx = outputs['timeRxMode'] ,\
											idle = outputs['timeIdleMode'] ,\
											periodLength = time * pow(10,-3),\
											name = "SIGFOX Global Timings (s)")
		outputs['timeSleepMode'] = globalTimings.sleep
		
		#s * mW
		globalConso = globalTimings.tx * self.hardware.txConso \
					+ globalTimings.rx * self.hardware.rxConso \
					+ globalTimings.idle * self.hardware.cpuOnConso \
					+ globalTimings.sleep * self.hardware.sleepConso

		outputs['energyConsumption'] = globalConso # mJ

		if (globConf.SIGFOXDebug == True) :	
			print str(globalTimings)
			print self.hardware
			print "Consumption tx : " + str(globalTimings.tx * self.hardware.txConso)  + 'mJ'
			print "Consumption rx : " + str(globalTimings.rx * self.hardware.rxConso)  + 'mJ'
			print "Consumption idle : " + str(globalTimings.idle * self.hardware.cpuOnConso)  + 'mJ'
			print "Consumption sleep : " + str(globalTimings.sleep * self.hardware.sleepConso)  + 'mJ'
			print "Global conso : " + str(outputs['energyConsumption']) + " mJ"
			print "---- End energy --->"
				
		return outputs
	
	def __repr__(self):
		myStr = Techno.__repr__(self)
		myStr += "----- SIGFOX "+ self.version + " specific values -----\n"
		myStr += 'Timer Inter Paquet : ' + str(self.TiP) + ' us\n'
		myStr += 'Time Inter transmission : ' + str(self.TiTx) + ' us\n'
		return myStr
		
		
if __name__ == '__main__' : 	
	mySIGFOX = SIGFOX(0.1,'oldPaper',"100bps")
	print mySIGFOX
	
	print "Max Packet time : " + str(mySIGFOX.maxPacketTime()*pow(10,-6)) + " s"
	print "10 bytes packt time : " + str(mySIGFOX.dataPacketTime(80)*pow(10,-6)) + " s"
	
	print "Capacity testing during one day : "
	from pprint import pprint	
	pprint(mySIGFOX.capacityOutputs(time = 86400000))