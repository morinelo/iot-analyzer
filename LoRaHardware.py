from Hardware import Hardware

class LoRaHardware(object):

	list = []

	@staticmethod
	def getList() :
		LoRaHardware.list.append(SX1272())
		LoRaHardware.list.append(SX1272Paper())
		LoRaHardware.list.append(oldPaper())
		LoRaHardware.list.append(compMonton())
		return LoRaHardware.list

# -------------- BLE HW Implementation --------------
def SX1272():
	# SX1272 Radio + (CPU consumption = greennet DS CPU)
	# + 20dBm TX, #LnA boost on rx (10.5 else)
	voltage = 3.3 #V
	cpuOn =  0.185*12*(3.2) #GN Value
	return Hardware({	'name' : 'SX1272' , 
		 				'rx' : cpuOn + 11.2 * voltage, 
		 				'tx' : cpuOn + 125 * voltage, 
		 				'cpu' : cpuOn,
		 				'sleep' :  0.44*pow(10,-3)*(3.2) })
def SX1272Paper():
	# SX1272 Radio + (CPU consumption = greennet CPU)
	voltage = 3.3 #V
	cpuOn =  0.185*12*(3.2) #GN Value
	return Hardware({	'name' : 'SX1272Paper' , 
		 				'rx' : cpuOn + 11.2 * voltage, 
		 				'tx' : cpuOn + 125 * voltage, 
		 				'cpu' : cpuOn,
		 				'sleep' :  1.8*pow(10,-3)*(3.2) })
		 				
def oldPaper():
	# sleep = smartmeshIP
	voltage = 3.3 #V
	cpuOn =	0.185*12*(3.2) #GN Value
	return Hardware({	'name' : 'oldPaper' , 
		 				'rx' : cpuOn + 11.2 * voltage, 
		 				'tx' : cpuOn + 125 * voltage, 
						'cpu' : cpuOn,
						'sleep' : 1.2 * pow(10,-3) * (3.6)}) 	

def compMonton():
	# sleep = smartmeshIP
	voltage = 3 #V
	return Hardware({	'name' : 'compMonton' , 
		 				'rx' : 10 * voltage, 
		 				'tx' : 45 * voltage, 
						'cpu' : 2,
						'sleep' : 0}) 
			
if __name__ == '__main__' :
	HWList = LoRaHardware.getList()
	for hw in HWList :
		print hw