import conf
import globConf
from math import log

if __name__ == "__main__" : 
	import sys
	if (len(sys.argv) < 3) : #i.e. no special param
		raise UserWarning("Usage : python simulator.py <mode> <dat_folder>" )
	else :
		mode = sys.argv[1]
		datPath = sys.argv[2]
	
	print "\t\t\t *-------------*"
	print "\t\t\t     WELCOME ! "
	print "\t\t\t *-------------*"
	print "This simulator emulates nodes behavior using different IoT protocols."
	print "Variables' name is fitting the paper, please refer to it if needed."
	print "It will compute Lt = f(ra) for a fixed amount of energy."
	print "You'll find a configuration file (conf.py), feel free to edit it.\n"
	print "For now the configuration, is the following : "
	#TODO
	print "*** ----------------------------------------------- ***"
	print "TODO : nice print of conf.py file :)"
	print "*** ----------------------------------------------- ***"

	technologies = conf.initializingTechnos()
	technoNames = [techno.name for techno in technologies]				
	if (globConf.simuDebug == True) : 
		print technoNames
	
	for timeUnit in conf.TAs :
		filename = str(datPath)+mode+str(timeUnit)+".dat"
		if (globConf.simuDebug == True) : 
			print filename
		file = open(filename,"w")
		header = "#data,  "+ ",  ".join([techno.name for techno in technologies]) + "\n"
		file.write(header)
		
		#Adding capacity max to dataList for each techno 
		from copy import deepcopy
		dataList = deepcopy(conf.rangeData)
		for techno in technologies : 
			maxCapa = techno.capacityOutputs(timeUnit)
			data = (maxCapa['totalData']/8) * pow(10,-3)
			if (data < max(dataList)) and (data not in dataList) and (data > 0): 
				dataList.append(data)
		dataList.sort()	

		#Fill file
		for data in dataList :  #data is in kbytes per time unit
			if (globConf.simuDebug == True) : 
				myStr = "****************\nTime to consume " + str(globConf.E0)
				myStr += " mJ with an applicatif rate of " + str(data) 
				myStr += " kbytes per " + str(timeUnit*pow(10,-3)) + " s"
				print myStr
			
			gnuplotLine = str(data) + ","
			lifetimeDict = dict.fromkeys(technoNames,0)
			for techno in technologies :
				#Computing lifetime 
				NRGperTU = techno.optimizedEnergy(data*8*pow(10,3), timeUnit)
				nbTUperGranularity = float(globConf.granularity * pow(10,3)) / float(timeUnit)
				NRGperGranularity = nbTUperGranularity * NRGperTU
				
				if mode == "lifetime" :
					batteryLeakagePerGranularity = globConf.batteryLeakage * globConf.granularity / (365*24*3600)
					Ecutoff = globConf.E0 * globConf.cutoffThreshold
					if (globConf.simuDebug == True) : 
						myStr= str(techno.name) + ' : '  + str(NRGperTU) + " mJ per "+ str(timeUnit) + "ms"
						myStr+= " (eq : " + str(NRGperGranularity) + " mJ/ "+str(globConf.granularity)+"s)"
						print myStr
						print "Battery leakage per " + str(globConf.granularity)\
								 + "s : " + str(batteryLeakagePerGranularity)
					if NRGperTU != float('inf') :
						#We have an arithmetico-geometric sequence
						# E(t+1) = a * E(t) + b with :
						a = float(1 - batteryLeakagePerGranularity)
						b = float(- NRGperGranularity)
						#Hence, E(t) = a^t * (E(0) - r) + r 
						# with :
						r = b / (1 - a) 
						#We look for t so that E(t) = Ecutoff
						lifetimeGranu = log(  (Ecutoff - r) / (globConf.E0 - r) , a)
						lifetimeDict[techno.name]  = lifetimeGranu * globConf.granularity * pow(10,3) #s
					else : 
						lifetimeDict[techno.name] = 0
					
					#Filling .dat File 
					if timeUnit < (1000) :
						#(lifetime in months)
						gnuplotLine += str(lifetimeDict[techno.name]*pow(10,-3)/(3600*24*30.5)) + "," 
					else :
						#(lifetime in years)
						gnuplotLine += str(lifetimeDict[techno.name]*pow(10,-3)/(3600*24*30.5*12)) + ","

				elif mode == "energyVaryingSaTa" or  mode == "avgBitCost" :
					NRGperSecond = NRGperGranularity / globConf.granularity #mJ/s
					NRGperSecondperUsefulBit = NRGperSecond / float(data*8*pow(10,3)) #average mJ/s/bit
					
					if (globConf.simuDebug == True) :
						print "Techno " + str(techno.name)
						print "Energy consumption : " + str(NRGperGranularity) + " mJ/ "+str(globConf.granularity)+"s"
						print "<=> " + str(NRGperSecond) + " mJ/s"
						print "Average Energy consumption : " + str(NRGperSecond) + " mJ/s for "+str(data*8*pow(10,3))+"bits"
						print "<=> " + str(NRGperSecondperUsefulBit) + " mJ/s/bit"
					
					if mode == "energyVaryingSaTa" :
						gnuplotLine += str(NRGperSecond) + "," 
					elif mode == "avgBitCost" :
						gnuplotLine += str(NRGperSecondperUsefulBit) + "," 
					else :	
						raise ValueError("Impossible mode condition")

			if (globConf.simuDebug == True) : 
				if mode == "lifetime" :
					print "Lifetime in ms"
					from pprint import pprint 
					pprint(lifetimeDict)
				print "--> gnuplot line : " + str(gnuplotLine)
				print "****************\n"
			gnuplotLine += "\n"
			file.write(gnuplotLine)
		file.close()		