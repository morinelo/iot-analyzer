#------- Environnement variables -------
simulationName = 'Fig15_0ppm'

#------- Execution variables -------
#clockAccuracy is expressed as (ppmRX - ppmTX) (ppm)
clockAccuracy = 'inf'
#T_a interval for wich we compute the curves (ms)
TAs = [10000]
#rangeData is the abscisse of the computed dot for curves (kbytes)
rangeData = [0.050,0.1,0.15]
#PDR is the AVERAGE packet delivery ratio
# ex: 80% of PDR means we compute the needed energy to transmit 5 packets 
# as the one to actually send 6 packets
PDR = 1

def initializingTechnos():
	""""
	Please uncomment the technologie you want to compare, 
	along with the HW you want to use for comparison
	"""
	technoList = []

	from fifteenDotFour import FifteenDotFour
	fifteenDotFour = FifteenDotFour('telosB')
	technoList.append(fifteenDotFour)
		
	from BLE import BLE
	BLE = BLE('BLE112','4.0')
	technoList.append(BLE)
	
	return technoList
		
if __name__ == "__main__" :
	print "TA_VALUES " + "  ".join([str(ta) for ta in TAs])
	print "NAME " + simulationName