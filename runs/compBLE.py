#------- Environnement variables -------
simulationName = 'compBLE'

#------- Execution variables -------
#clockAccuracy is expressed as (ppmRX - ppmTX) (ppm)
clockAccuracy = 'inf'
#T_a interval for wich we compute the curves (ms)
TAs = [10,1000,100000,86400000] 
#rangeData is the abscisse of the computed dot for curves (kbytes)
rangeData = [0.01,0.02,0.05,0.1,0.25,0.5,1,1.680,2,5,10] 
#PDR is the AVERAGE packet delivery ratio
# ex: 80% of PDR means we compute the needed energy to transmit 5 packets 
# as the one to actually send 6 packets
PDR = 0.8

def initializingTechnos():
	""""
	Please uncomment the technologie you want to compare, 
	along with the HW you want to use for comparison
	"""
	technoList = []

	from BLE import BLE
	BLE2 = BLE('newPaper','5.0', phyRate5=0.125)
	technoList.append(BLE2)

	from BLE import BLE
	BLE2 = BLE('newPaper','5.0',phyRate5=0.500)
	technoList.append(BLE2)

	from BLE import BLE
	BLE2 = BLE('newPaper','5.0',phyRate5=1)
	technoList.append(BLE2)

	from BLE import BLE
	BLE2 = BLE('newPaper','5.0')
	technoList.append(BLE2)
	
	from fifteenDotFour import FifteenDotFour
	B154 = FifteenDotFour('newPaper')
	technoList.append(B154)
	
	from fifteenDotFour import FifteenDotFour
	B154 = FifteenDotFour('newPaper', improved = True)
	technoList.append(B154)

	return technoList
		
if __name__ == "__main__" :
	print "TA_VALUES " + "  ".join([str(ta) for ta in TAs])
	print "NAME " + simulationName