#Set the TSCH KA interval at 3825 ms (twice per SF in SmartmeshIP)
#------- Environnement variables -------
simulationName = 'Fig15_0ppm'

#------- Execution variables -------
#clockAccuracy is expressed as (ppmRX - ppmTX) (ppm)
clockAccuracy = 'inf'
#T_a interval for wich we compute the curves (ms)
TAs = [1000000]
#rangeData is the abscisse of the computed dot for curves (kbytes)
rangeData = [0.012]
#PDR is the AVERAGE packet delivery ratio
# ex: 80% of PDR means we compute the needed energy to transmit 5 packets 
# as the one to actually send 6 packets
PDR = 1

def initializingTechnos():
	""""
	Please uncomment the technologie you want to compare, 
	along with the HW you want to use for comparison
	"""
	technoList = []

	from LoRa import LoRa
	LoRaEUMinHighDC = LoRa(1,'compMonton',"EuMin")			
	technoList.append(LoRaEUMinHighDC)
	
	LoRaEUMaxHighDC = LoRa(1,'compMonton',"EuMax")
	technoList.append(LoRaEUMaxHighDC)
 		
	from SIGFOX import SIGFOX
	SIGFOXEUMinHighDC = SIGFOX(1,'oldPaper',"100bps")
	technoList.append(SIGFOXEUMinHighDC)

	return technoList
		
if __name__ == "__main__" :
	print "TA_VALUES " + "  ".join([str(ta) for ta in TAs])
	print "NAME " + simulationName