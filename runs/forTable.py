#------- Environnement variables -------
simulationName = 'ForTable'

#------- Execution variables -------
#clockAccuracy is expressed as (ppmRX - ppmTX) (ppm)
clockAccuracy = 80
#T_a interval for wich we compute the curves (ms)
TAs = [10,1000,100000,86400000] 
#rangeData is the abscisse of the computed dot for curves (kbytes)
rangeData = [0.01,0.02,0.05,0.1,0.25,0.5,1,1.680,2,5,10] 
#PDR is the AVERAGE packet delivery ratio
# ex: 80% of PDR means we compute the needed energy to transmit 5 packets 
# as the one to actually send 6 packets
PDR = 0.8

def initializingTechnos():
	""""
	Please uncomment the technologie you want to compare, 
	along with the HW you want to use for comparison
	"""
	technoList = []

	from BLE import BLE
	BLE2 = BLE('newPaper','5.0', clockDrift = clockAccuracy)
	technoList.append(BLE2)

	from fifteenDotFour import FifteenDotFour
	B154 = FifteenDotFour('newPaper', clockDrift = clockAccuracy)
	technoList.append(B154)
	
	from TSCH import TSCH
	oldTSCH = TSCH('newPaper', drift = clockAccuracy)
	technoList.append(oldTSCH)
	TSCH = TSCH('newsmartMeshIP', drift = clockAccuracy)
	technoList.append(TSCH)
	
	from PSM import PSM
	PSM11 = PSM('oldPaper','11', clockDrift = clockAccuracy)
	technoList.append(PSM11)
	
	#1MHz ah technologies
	from ah import ah10_1
	ahTenOne = ah10_1('oldPaper20Rx', CD = clockAccuracy)
	technoList.append(ahTenOne)
	
	#2MHz ah technologies
	from ah import ah8_2
	ahEightTwo = ah8_2('oldPaper100Rx', CD = clockAccuracy)
	technoList.append(ahEightTwo)

	from ah import ah9_16
	ahNineSixteen = ah9_16('oldPaper200Rx', CD = clockAccuracy)
	technoList.append(ahNineSixteen)
		
	from LoRa import LoRa
	LoRaEUMinLowDC = LoRa(0.1,'oldPaper',"EuMin")
	technoList.append(LoRaEUMinLowDC)
	LoRaEUMinHighDC = LoRa(1,'oldPaper',"EuMin")			
	technoList.append(LoRaEUMinHighDC)
	
	LoRaEUMaxLowDC = LoRa(0.1,'oldPaper',"EuMax")
	technoList.append(LoRaEUMaxLowDC)
	LoRaEUMaxHighDC = LoRa(1,'oldPaper',"EuMax")
	technoList.append(LoRaEUMaxHighDC)
	
	
	
	from SIGFOX import SIGFOX
	SIGFOXEUMinLowDC = SIGFOX(0.1,'oldPaper',"100bps")
	technoList.append(SIGFOXEUMinLowDC)
	SIGFOXEUMinHighDC = SIGFOX(1,'oldPaper',"100bps")			
	technoList.append(SIGFOXEUMinHighDC)
	
	SIGFOXEUMaxLowDC = SIGFOX(0.1,'oldPaper',"1000bps")
	technoList.append(SIGFOXEUMaxLowDC)
	SIGFOXEUMaxHighDC = SIGFOX(1,'oldPaper',"1000bps")
	technoList.append(SIGFOXEUMaxHighDC)
	
	
	return technoList
		
if __name__ == "__main__" :
	print "TA_VALUES " + "  ".join([str(ta) for ta in TAs])
	print "NAME " + simulationName