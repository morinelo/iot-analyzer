#------- Environnement variables -------
simulationName = "Fig14_DiffHW"

#------- Execution variables -------
#clockAccuracy is expressed as (ppmRX - ppmTX) (ppm)
clockAccuracy = 'inf'
#T_a interval for wich we compute the curves (ms)
TAs = [1000,100000] 
#rangeData is the abscisse of the computed dot for curves (kbytes)
rangeData = [0.01,0.02,0.05,0.1,0.25,0.5,1,2,5,10] 
#PDR is the AVERAGE packet delivery ratio
# ex: 80% of PDR means we compute the needed energy to transmit 5 packets 
# as the one to actually send 6 packets
PDR = 1

def initializingTechnos():
	""""
	Please uncomment the technologie you want to compare, 
	along with the HW you want to use for comparison
	"""
	technoList = []

	from BLE import BLE
	BLE1 = BLE('nRF51822','5.0')
	technoList.append(BLE1)
	BLE2 = BLE('BLE112','5.0')
	technoList.append(BLE2)
	BLE3 = BLE('BlueNRG','5.0')
	technoList.append(BLE3)
	BLE4 = BLE('newPaper','5.0')
	technoList.append(BLE4)

	from fifteenDotFour import FifteenDotFour
	B154_1 = FifteenDotFour('greennet')
	technoList.append(B154_1)
	B154_2 = FifteenDotFour('telosB')
	technoList.append(B154_2)
	B154_3 = FifteenDotFour('smartMeshIP')
	technoList.append(B154_3)
	B154_4 = FifteenDotFour('newPaper')
	technoList.append(B154_4)
		
	from PSM import PSM
	PSM11_1 = PSM('G2M5477','11')
	technoList.append(PSM11_1)
	PSM11_2 = PSM('RTX4100','11')
	technoList.append(PSM11_2)
	PSM11_3 = PSM('classicalWifi','11')
	technoList.append(PSM11_3)
	PSM11_4 = PSM('MAX2830','11')
	technoList.append(PSM11_4)
	PSM11_5 = PSM('oldPaper','11')
	technoList.append(PSM11_5)
	
	
	
	return technoList
		
if __name__ == "__main__" :
	print "TA_VALUES " + "  ".join([str(ta) for ta in TAs])
	print "NAME " + simulationName