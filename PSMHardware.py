from Hardware import Hardware

class PSMHardware(object):

	list = []

	@staticmethod
	def getList() :
		PSMHardware.list.append(G2M5477())
		PSMHardware.list.append(RTX4100())
		PSMHardware.list.append(MAX2830())
		PSMHardware.list.append(classicalWifi())
		PSMHardware.list.append(oldPaper())
		return PSMHardware.list

# -------------- BLE HW Implementation --------------
def G2M5477(): #Tozlu's Paper (+18 dBm)
	voltage = 3.3 #V
	return Hardware({	'name' : 'G2M5477' , 
		 				'rx' :  51.5* voltage, 
		 				'tx' :  212* voltage, 
		 				'cpu' : 20*voltage,
		 				'sleep' : 4*pow(10,-3)*voltage})
		 				
def RTX4100(): # DS p.21 (+17 dBm)
	voltage= 3.5 #V
	return Hardware({	'name' : 'RTX4100' , 
		 				'rx' : 100 * voltage, 
		 				'tx' : 300 * voltage, 
		 				'cpu' : 2.6* voltage,
		 				'sleep' : 2.7 * pow(10,-3) * voltage}) 
		 			
def MAX2830(): # Datasheet (+17dBm)
	voltage= 3.3 #V
	return Hardware({	'name' : 'MAX2830' , 
		 				'rx' : 62 * voltage, 
		 				'tx' : 212 * voltage, 
		 				'cpu' : 28 * voltage ,
		 				'sleep' : 20 * pow(10,-3) * voltage}) 

def classicalWifi():#STM32 Wifi Module (+18dBm)
	voltage= 3.3 #V
	return Hardware({	'name' : 'classicalWifi' , 
		 				'rx' : 105 * voltage, 
		 				'tx' : 344 * voltage, 
		 				'cpu' : 26 * voltage ,
		 				'sleep' : 43 * pow(10,-3) * voltage}) 	

def oldPaper():
	return Hardware({	'name' : 'oldPaper' , 
						'rx' : 170, 
						'tx' : 699.6, 
						'cpu' : 9.1,
						'sleep' : 9.45 * pow(10,-3)}) 	

						
if __name__ == '__main__' :
	HWList = PSMHardware.getList()
	for hw in HWList :
		print hw
