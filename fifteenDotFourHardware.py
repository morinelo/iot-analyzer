from Hardware import Hardware

class fifteenDotFourHardware(object):

	list = []

	@staticmethod
	def getList() :
		fifteenDotFourHardware.list.append(greennetDS())
		fifteenDotFourHardware.list.append(greennet())
		fifteenDotFourHardware.list.append(telosB())
		fifteenDotFourHardware.list.append(smartMeshIP())
		fifteenDotFourHardware.list.append(oldPaper())
		fifteenDotFourHardware.list.append(newPaper())
		fifteenDotFourHardware.list.append(newsmartMeshIP())
		fifteenDotFourHardware.list.append(newOpenMoteSTM())
		return  fifteenDotFourHardware.list

# -------------- 802.15.4 HW Implementation --------------
def greennetDS():
	voltage = 3.0 #V
	cpuOn = 0.185*12*voltage # # CPU On (0,185 mA/MHz) * freq (12 MHz) 
	return Hardware({	'name' : 'greennetDatasheet' , 
		 				'rx' : cpuOn + 4.5 * voltage, 
		 				'tx' : cpuOn + 4.9 * voltage, 
		 				'cpu' : cpuOn,
		 				'sleep' : 0.44*pow(10,-3)*voltage})		 				
def greennet():
	voltage= 3.2 #V
	cpuOn = 0.185*12*voltage # # CPU On (0,185 mA/MHz) * freq (12 MHz) 
	return Hardware({	'name' : 'greennet' , 
		 				'rx' : cpuOn + 3.8 * voltage, 
		 				'tx' : cpuOn + 5.6 * voltage, 
		 				'cpu' : cpuOn,
		 				'sleep' : 1.8*pow(10,-3)* voltage}) 
		 				
def telosB():
	#+2dBm, Greennet Value for cpu and sleep
	cpuOn = 41
	return Hardware({	'name' : 'telosB' , 
		 				'rx' : cpuOn + 38, 
		 				'tx' : cpuOn + 35, 
		 				'cpu' : cpuOn ,
		 				'sleep' : 15 * pow(10,-3) }) 

def smartMeshIP() :
	voltage= 3.6 #V
	cpuOn = 0.176*7.37*voltage# CPU ON (0.176 mA/MHz) * 7.37MHz  (DS)
	return Hardware({	'name' : 'smartMeshIP' , 
		 				'rx' : cpuOn + 4.5 * voltage, 
		 				'tx' : cpuOn + 5.4 * voltage, 
		 				'cpu' : cpuOn,
		 				'sleep' : 1.2*pow(10,-3)* voltage})
def newsmartMeshIP():
	#Taking into account Psleep of BLE112 (to compare with oldPaper min-Energy)
	voltage= 3.6 #V
	return Hardware({	'name' : 'newsmartMeshIP' , 
						'rx' :  4.5 * voltage, 
						'tx' :  5.4 * voltage, 
						'cpu' :  0.176*7.37*voltage,
						'sleep' :  3.24 * pow(10,-3)}) 	

def newOpenMoteSTM():
	#Measured value (Realistic TSCH model paper)
	return Hardware({	'name' : 'newOpenMoteSTM' , 
						'rx' :  11.6, 
						'tx' :  13.7, 
						'cpu' :  32,
						'sleep' :  14*pow(10,-3)}) 		 				
		 					 				
		 				
		 				
def oldPaper():
	return Hardware({	'name' : 'oldPaper' , 
						'rx' : 19.26, 
						'tx' : 24.384, 
						'cpu' : 7.104,
						'sleep' : 3.24 * pow(10,-3)}) 	

def newPaper():
	return Hardware({	'name' : 'newPaper' , 
						'rx' : 19.26, 
						'tx' : 24.11, 
						'cpu' : 4.67,
						'sleep' : 3.24 * pow(10,-3)}) 

												
if __name__ == '__main__' :
	HWList = fifteenDotFourHardware.getList()
	for hw in HWList :
		print hw