from Hardware import Hardware

class SIGFOXHardware(object):

	list = []

	@staticmethod
	def getList() :
		SIGFOXHardware.list.append(TD1202())
		SIGFOXHardware.list.append(TD1202Paper())
		SIGFOXHardware.list.append(oldPaper())
		return SIGFOXHardware.list

# -------------- SIGFOX HW Implementation --------------
#Idle mode will never be used for SIGFOX 

def existant():
	#Tx@0dBm
	voltage = 2.5 #V
	cpuOn = 3.45 * voltage
	return Hardware({	'name' : 'existant' , 
		 				'rx' : cpuOn + 7 * voltage, 
		 				'tx' : cpuOn + 5 * voltage, 
		 				'cpu' : cpuOn,
		 				'sleep' :  0.1*pow(10,-3)* voltage })
  
def TD1202():
	#Tx@14dBm
	voltage = 3.0 #V
	return Hardware({	'name' : 'TD1202' , 
		 				'rx' : 13 * voltage, 
		 				'tx' : 49 * voltage, 
		 				'cpu' : 3*voltage,
		 				'sleep' :  1.3*pow(10,-3)* voltage })
 
def TD1202Paper():
	#Tx@14dBm
	voltage = 3.0 #V
	return Hardware({	'name' : 'TD1202Paper' , 
		 				'rx' : 13 * voltage, 
		 				'tx' : 49 * voltage, 
		 				'cpu' : 3*voltage,
		 				'sleep' :  1.7*pow(10,-3)* voltage })

def nextGen(): 
	#Tx@14dBm
	voltage = 1.2 #V
	cpuOn = 0.275*16*voltage 	#Taken from ???
	return Hardware({	'name' : 'nextGen' , 
		 				'rx' : cpuOn + 12, 
		 				'tx' : cpuOn + 120, 
		 				'cpu' : cpuOn,
		 				'sleep' : 1*pow(10,-3) })		 				
		 					 				
def oldPaper():
	# Tx@14dBm
	# sleep = smartmeshIP
	# cpuOn = GN Value
	voltage = 3.0 #V
	return Hardware({	'name' : 'oldPaper' , 
		 				'rx' : 13 * voltage, 
		 				'tx' :  49 * voltage, 
						'cpu' : 0.185*12*(3.2) ,
						'sleep' : 1.2 * pow(10,-3) * (3.6)}) 	
						
if __name__ == '__main__' :
	HWList = SIGFOXHardware.getList()
	for hw in HWList :
		print hw