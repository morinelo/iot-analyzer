from Hardware import Hardware

class BLEHardware(object):

	list = []

	@staticmethod
	def getList() :
		BLEHardware.list.append(nRF51822())
		BLEHardware.list.append(BLE112())
		BLEHardware.list.append(BlueNRG())
		BLEHardware.list.append(oldPaper())
		BLEHardware.list.append(newPaper())
		return BLEHardware.list

# -------------- BLE HW Implementation --------------
def nRF51822():
	#DC-DC / 0dBm
	voltage = 3.0 #V
	cpuOn = 0.275*16*voltage # 0.275 mA/MHz (read from flash) * CPU Freq (16MHz)
	return Hardware({	'name' : 'nRF51822' , 
		 				'rx' : cpuOn + 9.7 * voltage, 
		 				'tx' : cpuOn + 8 * voltage, 
		 				'cpu' : cpuOn,
		 				'sleep' : 2.6*pow(10,-3)*voltage})
def BLE112():
	voltage= 3.6 #V
	return Hardware({	'name' : 'BLE112' , 
		 				'rx' : 25 * voltage, 
		 				'tx' : 27 * voltage, 
		 				'cpu' : 7.6* voltage,
		 				'sleep' : 0.9 * pow(10,-3) * voltage}) 
		 
def BlueNRG():
	#+2dBm, Greennet Value for cpu and sleep
	voltage= 3.0 #V
	cpuOn = 0.185*12* 3.2 
	return Hardware({	'name' : 'BlueNRG' , 
		 				'rx' : cpuOn + 7.3 * voltage, 
		 				'tx' : cpuOn + 8.2 * voltage, 
		 				'cpu' : cpuOn ,
		 				'sleep' : 2 * pow(10,-3) * 3.2}) 
		 				
def oldPaper():
	return Hardware({	'name' : 'oldPaper' , 
						'rx' : 19.26, 
						'tx' : 24.384, 
						'cpu' : 7.104,
						'sleep' : 3.24 * pow(10,-3)}) 	
						
def newPaper():
	return Hardware({	'name' : 'newPaper' , 
						'rx' : 19.26, 
						'tx' : 24.11, 
						'cpu' : 4.67,
						'sleep' : 3.24 * pow(10,-3)}) 

														
if __name__ == '__main__' :
	HWList = BLEHardware.getList()
	for hw in HWList :
		print hw