from Hardware import Hardware

class ahHardware(object):

	list = []

	@staticmethod
	def getList() :
		import PSMHardware
		ahHardware.list.append(PSMHardware.G2M5477())
		ahHardware.list.append(MasterThesis())
		ahHardware.list.append(oldPaper20Rx())
		ahHardware.list.append(oldPaper100Rx())
		ahHardware.list.append(oldPaper200Rx())
		return ahHardware.list

# -------------- BLE HW Implementation --------------
		 				
def MasterThesis(): 
	voltage= 3.0 #V
	return Hardware({	'name' : 'MasterThesis' , 
		 				'rx' : 135, 
		 				'tx' : 250, 
		 				'cpu' : 20 * voltage,
		 				'sleep' : 1.5 }) 	

def oldPaper20Rx():
	voltage= 3.0 #V
	cpuOn = 10 * voltage 
	return Hardware({	'name' : 'oldPaper20Rx' , 
						'rx' : cpuOn + 20 , 
						'tx' : 400, 
						'cpu' : cpuOn,
						'sleep' : 2.5 * pow(10,-3) * voltage }) 	

def oldPaper100Rx():
	voltage= 3.0 #V
	cpuOn = 10 * voltage 
	return Hardware({	'name' : 'oldPaper100Rx' , 
						'rx' : cpuOn + 100 , 
						'tx' : 400, 
						'cpu' : cpuOn,
						'sleep' : 2.5 * pow(10,-3) * voltage })
						
def oldPaper200Rx():
	voltage= 3.0 #V
	cpuOn = 10 * voltage 
	return Hardware({	'name' : 'oldPaper200Rx' , 
						'rx' : cpuOn + 200 , 
						'tx' : 400, 
						'cpu' : cpuOn,
						'sleep' : 2.5 * pow(10,-3) * voltage })  
						
						
if __name__ == '__main__' :
	HWList = ahHardware.getList()
	for hw in HWList :
		print hw