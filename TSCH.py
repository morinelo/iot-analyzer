from Techno import Techno
from math import ceil
from fuzzyFloor import fuzzyFloor
from consoModeTimings import consoModeTimings
import conf
import globConf

class TSCH(Techno):
	
	def __init__(self, hwname, drift = 'inf', clockAccuracy = 40) :
		#clockAccuracy needed to compute KA period
		symbDuration = 16.0 #us
		rate = 4 * ( 1 / symbDuration) #(Mbps)
		phyOvhd = 8 * symbDuration + (2 * 8) / rate #(us) 
		# 8 symb of Preamble + SFD (8 bits) + PHR (Frm len + reserved = 8 bits) 
		
		Techno.__init__(self, name = "TSCH", \
							maxPacketSize = 117, \
							PHYRate = rate, \
							PHYOvhd = phyOvhd, \
							MACOvhd = 64, \
							SixOvhd = 16,\
							clockDrift = drift,\
							platform_name = hwname)
		
		#Variables define in MAC PIB Table 52e (15.4e-2012) : all in us
		# (commented variables are not used for now)
		self.TsTxOffset = float(2120)
		self.TsRxOffset = float(1120)
# 		self.PGT = 2200 #Packet Guard Time (Useless :/)
		self.TSCH_GI = self.TsTxOffset - self.TsRxOffset #eq. of correct PGT
# 		self.TsTxAckDelay = 1000 
		self.TsRxAckDelay = 800  
 		self.AGT = 400.0 / 2 #Acknowledgment Guard Time 
		self.TSLen = 10 #(ms)
		
		self.clockDrift = drift
		self.KAPeriod = fuzzyFloor(self.TSCH_GI / \
									(clockAccuracy* 2 * pow(10,-6)) \
							* pow(10,-3)) # ms
		IELen = 7 #bits
		IEEID = 8 #bits
		IEType = 1 #bit
		IEListTerm = 16 # bits
		headerIESize = (IELen + IEEID + IEType + IEListTerm) / 8 #bytes
		NACKACKCorrTimeContentSize = 2 #bytes
		IETimeCorrSize = self.MACOvhd + \
				(headerIESize + NACKACKCorrTimeContentSize) * 8
		self.ackTime = self.PHYOvhd + IETimeCorrSize / self.PHYRate #640 (us)
			
	def getAvailableHWList(self):
		from fifteenDotFourHardware import fifteenDotFourHardware
		return fifteenDotFourHardware.getList()	
	
	def ackedTransmissionRxTime(self):
		return (self.ackTime + self.AGT)
	
	def ackedTransmissionIdleTime(self):
		if "new" in self.hardware.name :
			#All values in us (extrapolated from Realistic Model for TSCH)
			#----------------
			#Starting slot 
			startSlotTime = 300.0
			#Prepare data packet
			txDataPrepareTime = 1000.0
			#Time to start Radio in Tx Mode
			startTxTime = 300.0
			#Time to shut radio off
			entTxTime = 250.0
			#Time to start Radio in Rx Mode
			startRxTime = 250.0
			#Starting reception (check preamble)
			checkRxTime = 300.0
			#Time to handle packet reception (us)
			handleRxTime = 300.0 #average value, depends on packet size.
		
			cpuOnTime = startSlotTime \
						+ txDataPrepareTime + startTxTime + entTxTime \
						+ startRxTime + checkRxTime + handleRxTime
			if (globConf.TSCHDebug == True) :
				print "CPU On : " + str(cpuOnTime) + " us"
			return cpuOnTime
		else : 
			raise UserWarning
		
	def capacityOutputs(self, time, DC = 1):
		""" takes time in ms, return capa in bits
		"""
		transmittingAvailableTime = time * DC
		#Full slot tx
		nbFullSlot = fuzzyFloor(transmittingAvailableTime / self.TSLen)
		capaByFullSlot = nbFullSlot * self.maxPacketSize * 8
		#Half slot tx ? (cross multiplication for now) 
		sizeLastSlot = transmittingAvailableTime % self.TSLen
		ratio = float(sizeLastSlot) / self.TSLen
		
		capaBits = capaByFullSlot * ( nbFullSlot + ratio ) / (1/conf.PDR)
		return {'totalData' : capaBits}
		
	def txSlotTimings(self,dataSize) :
		''' This function returns the timings in the different mode for a 
		Tx slot (transmiting dataSize bits). All values are in us#'''
		txTime = self.dataPacketTime(dataSize)
		rxTime = self.ackedTransmissionRxTime()
		try :
			idleTime = self.ackedTransmissionIdleTime()
		except UserWarning : 
			txOrRxTime = txTime + rxTime
			idleTime =  self.TSLen * pow(10,3) - txOrRxTime
			
		return consoModeTimings( tx = txTime, \
								 rx = rxTime, \
								 idle = idleTime, \
								 periodLength = self.TSLen * pow(10,3),\
								 name = "tx",\
								 unit = "us")

	def energyPerTxSlot(self,dataSize):
		"""dataSize in bits"""
		txSlotTimings = self.txSlotTimings(dataSize) #us
		if globConf.TSCHDebug == True :
			print str(txSlotTimings)
		#us * mW = nJ 
		energy = txSlotTimings.tx * self.hardware.txConso \
				+ txSlotTimings.rx * self.hardware.rxConso \
				+ txSlotTimings.idle * self.hardware.cpuOnConso \
				+ txSlotTimings.sleep * self.hardware.sleepConso
		energy = energy * pow(10,-6) #convert to mJ
		if globConf.TSCHDebug == True :
			myStr = "\t(TxSlot) NRG to transmit " + str(dataSize) + " bits : " \
					+ str(energy) + " mJ"
			print myStr
		return energy 
		
	def synchroSlotTimings(self) :
		''' This function returns the timings in the different mode for a 
		synchronisation slot (no data). All values are in us#'''
		if self.clockDrift != 'inf' :
			self.guardInterval = self.KAPeriod * pow(10,3) \
									* (self.clockDrift * pow(10,-6))
		if globConf.GIDebug == True : 
			print "TSCH current GI : " + str(self.guardInterval) 
		rxTime = self.MACOvhd / self.PHYRate \
				+ self.PHYOvhd \
				+ self.TSCH_GI + self.guardInterval
		txTime = self.ackTime
		try :
			idleTime = self.ackedTransmissionIdleTime()
		except UserWarning : 
			txOrRxTime = txTime + rxTime
			idleTime =  self.TSLen * pow(10,3) - txOrRxTime
			
		return consoModeTimings( tx = txTime,\
								 rx = rxTime,\
								 idle = idleTime,\
								 periodLength = self.TSLen * pow(10,3), \
								 name = "synchro")
	
	def energyPerSynchroSlot(self):
		synchroSlotTimings = self.synchroSlotTimings() #us
		#us * mW = nJ 
		energy = synchroSlotTimings.tx * self.hardware.txConso  \
				+ synchroSlotTimings.rx * self.hardware.rxConso \
				+ synchroSlotTimings.idle * self.hardware.cpuOnConso \
				+ synchroSlotTimings.sleep * self.hardware.sleepConso
		energy = energy * pow(10,-6) #convert to mJ
		if globConf.TSCHDebug == True :
			print "\t(SynchroSlot) NRG per Synchro " + str(energy) + " mJ"
		return energy 

	def optimizedEnergy(self, data,time):
	 	""" This function compute the optimizedNRG for our case (data - bits, time - ms) : 
	 	choose right AP/DC values and compute the energy in this context
	 	"""
	 	if (globConf.TSCHDebug == True) :	
			print "--> Computing TSCH energy with parameters : "
			print "data : " + str(data) + " bits"
			print "time : "  + str(time) + " ms"
		
	 	#Compute number of necessary packets
		nbFullPckts = fuzzyFloor(data / (self.maxPacketSize * 8))
		leftoverSize = fuzzyFloor(data % (self.maxPacketSize * 8 ))#bits
		#Deducing nb of active slots
		nbActiveSlots = nbFullPckts * (1/conf.PDR)
		if (leftoverSize > 0) : 
			nbActiveSlots += 1
		
		timeToTx = nbActiveSlots * self.TSLen #ms
		
		if (globConf.TSCHDebug == True) :	
			myStr = str(nbFullPckts)+ " full packets needed + " + \
					str(leftoverSize) + " bits"
			print myStr
			print "Hence, nb Active slots (with PDR) : " + str(nbActiveSlots)
			print "Time to tx : " + str(timeToTx) + " ms"
		
		if time < timeToTx : # Beyond capacity 		
			if globConf.capaDebug == True :
				myStr = "/!\ TSCH does not support " + str(data*pow(10,-3)) 
				myStr += " kbits/"+ str(time) + "ms" 
				print myStr
			return float('inf')			
		
		#Timings
		nbSynchroEvents =  max((time / self.KAPeriod) - ceil(nbActiveSlots),0) 
														#sync by timeCorr
		timeToSynchro = nbSynchroEvents * self.TSLen #ms
	
		timeInSleep = (time - timeToTx - timeToSynchro) #ms
		if (globConf.TSCHDebug == True) :
			print "nbSynchroEvents : " + str(nbSynchroEvents)
			print "Time to synchro : " + str(timeToSynchro) + " ms"
			print "Time in sleep mode :" + str(timeInSleep) + " ms"
			
		#Energy consumption
		synchroConso = nbSynchroEvents * self.energyPerSynchroSlot()
		#For now, we assume no fragmentation needed
		transmitConso =  ( nbFullPckts \
						   * self.energyPerTxSlot(self.maxPacketSize * 8 ) \
						   + self.energyPerTxSlot(leftoverSize) ) \
						 * (1/conf.PDR)
			
		energy = transmitConso + synchroConso + \
				timeInSleep * pow(10,-3) * self.hardware.sleepConso

		if (globConf.TSCHDebug == True) :	
			print "Global Energy : " + str(energy) + " mJ"
					
		return energy
	
	def __repr__(self):
		myStr = Techno.__repr__(self)
		myStr += "----- TSCH specific values -----\n"
		myStr += 'TsTxOffset : ' + str(self.TsTxOffset) + ' us\n'
		myStr += 'TSCH Guard Time : ' + str(self.TSCH_GI) + ' us\n'
		myStr += 'TsRxAckDelay : ' + str(self.TsRxAckDelay) + ' us (equivalent SIFS)\n'
		myStr += 'Time Slot Length : ' + str(self.TSLen) + ' ms\n'
		myStr += 'Ack Duration: ' + str(self.ackTime) + ' us\n'	
		myStr += 'KA Period: ' + str(self.KAPeriod) + ' ms\n'
		return myStr
		
		
if __name__ == '__main__' : 
	from pprint import pprint		
	myTSCH = TSCH('newOpenMoteSTM')
	print myTSCH
	
	myTSCH.optimizedEnergy(myTSCH.maxPacketSize*8,10)
	