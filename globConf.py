# This file is the only one the user should modify regarding global variable for the script (dedicated run variable are found in runs/*.py files)

#------- Environnement variables -------
gnuplotPath = '/usr/local/bin/gnuplot'
pythonPath = '/Library/Frameworks/Python.framework/Versions/2.7/bin/python'
latexmkPath = '/Library/TeX/texbin/latexmk'

#------- Debug variables -------
capaDebug = False
synchroTechConsoDebug = False
GIDebug = False
synchroTechCapaFineDebug = False
synchroTechCapaDebug = False
simuDebug = False
TSCHDebug = False
LoRaDebug = False
SIGFOXDebug = False

#Auto debug variables :
synchroTechGIDebug = False
if GIDebug == True :
	synchroTechGIDebug = True
#------- Execution variables -------
#E0 is the initial battery energy in mJ
E0 = 13500000 #(mJ) <-- 2*AAA = 2*1250(mAh)*1,5(V) = 3,750 Wh = 13,5kJ 
#granularity defines the results granularity, expressed in s
granularity = 3600
#batteryLeakage is expressed as a yearly percentage
batteryLeakage = 5.0/100
#cutoff energy is expressed as a percentage of the full battery capacity
cutoffThreshold = 10.0/100

		
if __name__ == "__main__" :
	print "PYTHON " + pythonPath
	print "GNUPLOT " + gnuplotPath
	print "LATEXMK " + latexmkPath
	print "E0 " + str(E0)