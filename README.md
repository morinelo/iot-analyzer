# Iot-Analyzer
Creator: E. Morin Date: March 15th, 2017

This project contains the```iot-analyzer ```that compares different IoT technologies in an IEEE Access Journal Paper [1]. 

The project compares the expected lifetime, energy consumption, and average bit cost of IoT devices operating in several wireless networks: IEEE 802.15.4/e, Bluetooth Low Energy (BLE), IEEE 802.11 Power Saving Mode (PSM), IEEE 802.11ah, as well as in new emerging long-range technologies such as LoRa and SIGFOX. 

To compare all technologies on an equal basis, we have developed an``` iot-analyzer ```that computes the energy consumption for a given protocol based on the power required in a given state (Sleep, Idle, Tx, Rx) and the duration of each state.
We consider the case of an energy constrained node that uploads data to a sink,
analyzing the physical (PHY) layer under Medium Access Control (MAC)
constraints, and assuming IPv6 traffic whenever possible. The study takes into
account the energy spent in retransmissions due to corrupted frames and
collisions as well as the impact of imperfect clocks. We believe that these comparisons will give all users of IoT technologies indications about the technology that best fits their needs from the energy consumption point of view. Our analyzer will also help IoT network designers to select the right MAC parameters to optimize the energy consumption for a given application.

## Getting Started

The instructions below will tell you how to get a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

To use this analyzer, you need: 

1. **python version 2.7** or above:
Use ``` python --version ``` to determine which version you are using.
To install python, see [python.org](https://wiki.python.org/moin/BeginnersGuide/Download).
2. **gnuplot version 4.6** or above with ```lua tikz``` terminal enabled.
   Use ``` gnuplot --version ```  to determine which version you are using. 
   To check if ```lua tikz``` terminal is enabled: enter interactive gnuplot with command```gnuplot```, type ```set terminal``` to see a list of the available terminals. Look for ```lua tikz```. 
   To install gnuplot from scratch, see [gnuplot.info](http://www.gnuplot.info/), do not forget to enable ```lua tikz``` terminal.
3. **latexmk** enabled to compile gnuplot generated tikz figures. See [Latexmk](http://personal.psu.edu/~jcc8/software/latexmk/) webpage.

Then, set the corresponding path in [globConf.py](globConf.py).

### Installing

The analyzer is coded in python called by a [Makefile](Makefile), hence you don't need to install anything else: just clone the project with the command 
``` git clone git@gitlab.imag.fr:morine/iot-analyzer.git``` and enjoy it!


To play with the``` iot-analyzer ```, you can use different GNU Make targets summarized by 
``` make man ``` and ```make help``` command.

#### Comparison script make global rules

 * ```all```: creates pdf plots for the targets listed at the top of [Makefile](Makefile) (default: all existing conf files)
 * ```paper```: creates pdf plots we used for the paper *Comparison of the Device Lifetime in Wireless Networks for the Internet of Things, IEEE Access, 2017*
 * ```clean```: removes temporary files
 * ```deepclean```: removes all files linked to a given scenario
 * ```help```: displays a help on how to use a specific target
 * ```man```: gets the manual page

#### Comparison script make specific rules
Usage: ````make <mode><conf>```` (no space in-between)                              
With:                                                          

* Available configuration files :   
                                               
 *  ```clockDriftNoPDR```: generate curves WITH clock drift and PDR=100% (Figure 20 of [1])
 *  ```compBLE```: generate curves to compare BLE 5.0 to BLE4.2
 *  ```compGreenNet```: generate curves to compare the analyzer output to the measurements of [2]
 *  ```compSMIPest```: generate curves to compare the analyzer output to the estimations of [3]
 *  ```compNieminen```: generate curves to compare the analyzer output to the measurements of [4]  
 *  ```compPoliba```: generate curves to compare the analyzer output to the measurements of [5]   
 *  ```diffHW  ```: generate curves to compare different hardware impact (Figure 15 of [1]).
 *  ```EH  ```: generate curves to estimate the feasibility of energy harvesting (Figure 21 of [1])
 *  ```forTable ```: generate curves used to fill the concluding table of [1]
 *  ```noClockDriftNoPDR ```:  generate curves WITHOUT clock drift and PDR=100% (Figure 16 of [1])
 * ``` noClockDriftPDR ```: generate curves WITHOUT clock drift and PDR < 100% (Figure 18 of [1])
* Available modes:                                                                                   
 *  ```lt ```: used to generate *lifetime* curves [ lifetime = f(applicative data rate) ] 
 *  ```nrg ```: used to generate *energy consumption* curves  [ energy_mJ = f(applicative data rate) ] 
 * ``` bc ```:  used to generate *average bit cost* curves [ energy per bit = f(applicative data rate) ] 

Example:
````make ltcompBLE```` (where mode=lt, conf=compBLE),````make nrgEH```` (where mode=nrg, conf=EH), ...

## Personalizing the analyzer

### Architecture of the code
A GNUMake meta-makefile ([Makefile](Makefile)) calls different configuration files in a row (all configuration files stored in [runs](runs/) folder). Depending on the ```<mode>``` and ```<conf>``` of the make call, this Makefile will copy the corresponding configuration file in a temporary ```conf.py``` at the project root, and then calls [Makefile.run](Makefile.run) GNUMake with the environment variable ```MODE``` set to the correct value.

[Makefile.run](Makefile.run) ```all``` target depends on the pdf file representing ```<mode>``` and ```<conf>``` chosen by the user.

The ```.pdf``` files depend on the corresponding ```.tex``` files, which result from the execution of a gnuplot script that plots the results of a given scenario ```.dat``` files. The ```.dat``` files represent the raw outputs of the execution of [simulator.py](simulator.py) (you can find them in ```Outputs/<simulationName>/dat```folder) .

[Simulator.py](Simulator.py) implements the algorithm presented in the paper [1] to compute the energy consumption, lifetime, or average bit cost of the technologies selected in the configuration (```conf.py```) file. 

The code is then organized in different classes for each technology, all inheriting from the Techno class (defined in [Techno.py](Techno.py) file). A subclass for all synchronized technologies also exists (defined in [SynchroTechno.py](SynchroTechno.py)  file). This subclass can be used for any technology that uses a synchronization frame of a parent to stay synchronized to the network (such as a beacon frame in 802.15.4 solution). 



### Creating a new comparison
To create a new comparison, create a new configuration file in [runs](runs/) folder. I advice you to first copy/paste an existing file and modify it to suit your needs.

**Warning:** The ```simulationName``` corresponds to the name of the plotting script used to generate the curves. 

### Plotting new curve types (gnuplot script)
As explained in the previous paragraph, the ```simulationName``` of your runFile corresponds to the name of the plotting script used to generate the curves. 
These plot scripts are stored in the dedicated [plotScripts](plotScripts/) folder, organized in different subfolders, corresponding to the ```<mode>``` you call.
Hence, you can create new plot types, inspired by the existing one, and calling them by setting the corresponding```simulationName```parameter in your new configuration file ([runs](runs/) folder) .

### Implementing a new technology

To create a new technology, just create a child of ```Techno```class, which at least implements the abstract methods. Then, add this techno in the```initializingTechnos()``` function of your configuration file (for instance : [runs/diffHW.py](runs/diffHW.py) file). Do not forget to modify the plot scripts to include your new technology.

## Authors

Elodie Morin

## License

This project is licensed under the BSD 3 License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Thanks to Andrzej Duda, Mickael Maman, and Roberto Guizzetti for the coding suggestions.
* Thanks to Pierre Brunisholz, Etienne Dublé, and Colin Chargy for their Makefile tutorials.
* Thanks to Henry-Jospeh Audeoud for the git support.

## Bibliography 

[1] E. Morin, M. Maman, R. Guizzetti, and A. Duda, "Comparison of the Device Lifetime in Wireless Networks for the Internet of Things", IEEE Access 2017 (forthcoming).

[2] L.-O. Varga, G. Romaniello, M. Vucˇinic ́, M. Favre, A. Banciu, R. Guizzetti, C. Planat, P. Urard, M. Heusse, F. Rousseau, O. Alphand, E. Dublé, and A. Duda, “GreenNet: an Energy Harvesting IP-enabled Wireless Sensor Network,” IEEE Internet of Things Journal, vol. 2, no. 5, pp. 412–426, 2015.

[3] Linear, “Dust Networks SmartMesh Power and Performance Estimator,” Accessed on 2017-1-10. [Online]. Available: http://www.linear.com/docs/42452.

[4] M. Siekkinen, M. Hiienkari, J. K. Nurminen, and J. Nieminen, “How Low Energy
is Bluetooth Low Energy? Comparative Measurements with ZigBee/802.15.4,” in WCNC
2012 Workshop on Internet of Things Enabling Technologies, 2012.

[5] B. Martinez, M. Montón, I. Vilajosana, and J. D. Prades, “The Power of Models: Modeling Power Consumption for IoT Devices,” IEEE Sensors Journal, vol. 15, no. 10, pp. 5777–5789, 2015.
