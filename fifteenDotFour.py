from SynchroTechno import SynchroTechno
from fifteenDotFourHardware import *

class FifteenDotFour(SynchroTechno):
	"""This class aims at modeling 802.15.4 functionning"""
	def __init__(self, hwname, clockDrift = 'inf', improved = False):
		symbolDuration = 16.0 #us
		rate = 8/(2*symbolDuration) #Mbps
		# 8 symb = 1 useful bit but OQPSK -> 2 bits per symb 	
		phyOvhd = (8 * symbolDuration) + (float(2 * 8) / rate)
		
		# Hwname should be in implemented 15.4Hardware List
		# Synchro period :  max BI = aBaseSFD * 2^14 
		#	with aBasedSFD = nbSymbinSFD * symbDuration * aNumSuperframeSlots
		SynchroTechno.__init__(	self,\
								name = "15.4",\
								platform_name = hwname,\
								maxPacketSize = 116,\
								PHYRate = rate,\
								PHYOvhd = phyOvhd,\
								MACOvhd = 72,\
								SixOvhd = 16,\
								CD = clockDrift, \
								synchroPeriod = 251658.240)

		if (improved) :
			#Test for BLE 5.0 equivalency
			self.maxPacketSize = 245
			self.PHYRate = 2

		self.SIFS = 12 * symbolDuration #us 
		self.LIFS = 40 * symbolDuration #us
		self.CW0 = 2 #Number of CCA
		self.CCADuration = 8 * symbolDuration #us
		self.backoff = 0 # We chose not to do the CSMA/CA 	

		# Beacon frame format (unit = byte)
		frameCtrl = 2 
		seqNum = 1 
		addressingField = 4 # 4/10
		securityHeader = 0 #0/5/6/10/14
		superframeSpecification = 2
		gtsField = 0 #variable
		pendingAddress = 0 #variable
		beaconPayload = 0 #variable
		fcs = 2
		#beaconSize in bits
		beaconMACFrameSize =  (frameCtrl + seqNum \
							+ addressingField + securityHeader \
							+ superframeSpecification + gtsField \
							+ pendingAddress + beaconPayload + fcs) * 8	

		self.beacon = beaconMACFrameSize / self.PHYRate + self.PHYOvhd # us
		self.ack = self.MACOvhd / self.PHYRate + self.PHYOvhd
	
	def getAvailableHWList(self):
		from fifteenDotFourHardware import fifteenDotFourHardware
		return fifteenDotFourHardware.getList()	
	
	def ackedTransmissionRxTime(self):
		return (self.ack + (self.CW0 * self.CCADuration) )
		
	def ackedTransmissionIdleTime(self):
		return (self.backoff + self.LIFS + self.SIFS)
	
	def APRxOvhd(self):
		return self.guardInterval + self.beacon
	
	def APIdleOvhd(self):
		return self.LIFS
	
	def synchroPeriodTimings(self, period):
		from consoModeTimings import consoModeTimings
		synchroTimings = consoModeTimings( 	tx = 0,\
											rx = self.APRxOvhd(),\
											idle=self.APIdleOvhd(),\
											periodLength = period,\
											name = "Synchro Period ")
		return synchroTimings
															
	def __repr__(self):
		myStr =	SynchroTechno.__repr__(self)
		myStr += "----- 802.15.4 specific values -----\n"
		myStr += 'SIFS : ' + str(self.SIFS) + ' us\n'
		myStr += 'LIFS : ' + str(self.LIFS) + ' us\n'
		myStr += 'CW0 : ' + str(self.CW0) + '\n'
		myStr += 'CCA Duration : ' + str(self.CCADuration) + ' us\n'
		myStr += 'Backoff : ' + str(self.backoff) + ' us\n'
		myStr += 'Ack Duration: ' + str(self.ack) + ' us\n'
		myStr += 'Beacon Duration : ' + str(self.beacon) + ' us\n'
		return myStr
		
				
#****************************************** Tests ******************************************		
if __name__ == '__main__' : 
	from pprint import pprint		
	my154 = FifteenDotFour('greennet')
	print my154
	
	print my154.dataPacketTime(220)

	
	
	print "\n *----- 15.4 Max Packet Cost Test -----*"
	print my154.computeMaxPacketCost()
	
	print "\n *----- 15.4 Capacity output test -----*"
	print "Computing maximum data to send within 300 s\n"
	print "--> Expected totalData with fragmentation : 45740992 bits : "
	print " Without fragmentation = 47 801 280 bits"
	pprint(my154.capacityOutputs(time = 300000))
	#WITHOUT FRAGMENTATION
	#We have one full synchro period and one "lastperiod"
	#During full period :
	# we can transmit 43210 full packets 
	# we have 2016 us left -> 0 bits of data
	#Last period :
	# we can transmit 8300 full packets 
	# we have 1376 us left -> 0 bits of data
	# = 47 801 280 bits 
	print "Computing maximum data to send within 10 ms\n"
	print "--> Expected totalData Without fragmentation = 9184 bits"
	pprint(my154.capacityOutputs(time = 10))

# TODO : 15.4 energy conso Test (when Fragmentation is handled)	
	print "\n *----- 15.4 energy consumption test -----*"
	print "Computing energy consumption (mJ) to handle 1148 bits per 10ms\n"
	#Be careful, commented results are not taking into account GI (disable it to check)
	pprint(my154.energyConsumption(amountOfData = 1148, time = 10))
	