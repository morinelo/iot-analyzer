from Techno import *
from consoModeTimings import consoModeTimings
from math import ceil
import conf
import globConf

class SynchroTechno(Techno) :
	""" Class defining a synchronous technology for the global consumption comparison """
	def __init__(self, name, maxPacketSize, PHYRate, PHYOvhd, MACOvhd, SixOvhd,\
					synchroPeriod, CD, platform_name = "greennet") :
		
		Techno.__init__(self, name, maxPacketSize, PHYRate, PHYOvhd, MACOvhd, SixOvhd, CD, platform_name)
		#Synchro
		self.synchroPeriod = synchroPeriod #ms
	
	def computeGuardInterval(self,sleepInterval):
		if (globConf.synchroTechGIDebug == True) :
			print "Sleeping interval : " + str(sleepInterval)
			print "clockDrift : " + str(self.clockDrift)
		k = int()
		clockDriftCalc = float()
		if (self.clockDrift < 0) :
			k = 0
			clockDriftCalc = self.clockDrift
		elif (self.clockDrift  ==0) :
			k = 1
			clockDriftCalc = 60
		elif (self.clockDrift == 'inf') :
			clockDriftCalc = k = 0
		else : #(self.clockDrift  > 0)
			k = 2	
			clockDriftCalc = self.clockDrift
	
		self.guardInterval = clockDriftCalc * pow(10,-6) * sleepInterval * k #us	
		if (globConf.synchroTechGIDebug == True) :	
			print str(self.name) + " new GI is : " + str(self.guardInterval) + ' us'
			
	def resetGuardInterval(self):
		self.guardInterval = 0	
		
	def APoverhead(self):
		return (self.APRxOvhd() + self.APIdleOvhd())
	
	def computeAPLengthOnePacket(self,dataLength): 
		return self.APoverhead() + dataLength + self.ackedTransmissionOvhd() #us
		
	def capaMaxDetermineNbFullPacketAndLeftoverSize(self, time):
		""" Time in us"""
		nbFullPackets = leftoversSize = 0 #Init
		if (time > 0) : 
			fullAckTransmissionTime = self.ackTransmissionTime(self.maxPacketSize *8)
			nbFullPackets = fuzzyFloor((time - self.APoverhead()) / fullAckTransmissionTime )
		
			deltaTime = (time - self.APoverhead()) % fullAckTransmissionTime 
			deltaData = deltaTime - self.totalOvhd() - self.ackedTransmissionOvhd()
			if (globConf.synchroTechCapaFineDebug == True) : 	
				print "\t Time to exchange data : " + str(time - self.APoverhead())
				print "\t Time to transmit leftovers : " + str(deltaTime)
				print "\t Acked Transmission Ovhd : " + str(self.ackedTransmissionOvhd())
				print "\t Time to transmit packet : " + str(deltaTime - self.ackedTransmissionOvhd())
				print "\t Time ovhd per packet : " + str(self.totalOvhd())
				print "\t Leftovers time once Ovhd removed : " + str(deltaData)
			if (deltaData > 0) :
				#Converting the time we have left into data size : 
				leftoversSize =  fuzzyFloor(deltaData * self.PHYRate) 
					#fuzzyFloor since nbBits can't be decimal number
			else : 
				#Not enough time
				leftoversSize =  0 
		
		totalData = nbFullPackets * self.maxPacketSize * 8 + leftoversSize
		
		output =  {	'nbFullPacket' : nbFullPackets,\
					'leftoversSize' : leftoversSize,\
					'totalData' : totalData }
		return output
	
	def capacityOutputs(self, time, DC = 1):
		"""This function computes the maximum tx capacity for a given DC during a :
		return outputs { 	'totalData' #bits , 
							'totalTime' #ms , 
							'capacityMax' #kbps } 
		"""
					
		period = min(time,self.synchroPeriod) #ms
		nbFullPeriod = fuzzyFloor(time / period) #fuzzyFloor ok since % after
		nbPeriod = nbFullPeriod
		# ----- Capacity output per full event/BI/superframe -----
		fullPeriodParam = self.capaMaxDetermineNbFullPacketAndLeftoverSize(period * pow(10,3))
		
		if (globConf.synchroTechCapaDebug == True) : 	
			from pprint import pprint
			print "Full Period Parameters :" 
			pprint(fullPeriodParam)
			print "Nb full period = " + str(nbFullPeriod)
			
		# --- Capacity brough by non-full connEvent ---- 
		lastPeriodSize = (time % period) * pow(10,3) # us
		if (lastPeriodSize > 0) : 
			nbPeriod = nbFullPeriod + 1
		if (globConf.synchroTechCapaDebug == True) : 	
			print "Last Period lengths " + str(lastPeriodSize) + " us" 
		lastPeriodParam = self.capaMaxDetermineNbFullPacketAndLeftoverSize(lastPeriodSize)

		#nbFullPackets
		nbFullPackets = fullPeriodParam['nbFullPacket'] * nbFullPeriod + lastPeriodParam['nbFullPacket'] 
		#nbTransmissions
		nbTransmissions = nbFullPackets
		if fullPeriodParam['leftoversSize'] > 0 :
			nbTransmissions += 1 	
		if lastPeriodParam['leftoversSize'] > 0 :
			nbTransmissions += 1
			
		nbTransmissionsCapaEq = nbTransmissions / (1/conf.PDR)
		
		if (globConf.synchroTechCapaDebug == True) :
			print "Taking into account a PDR of " + str(conf.PDR)
			print "nbFullPackets : " + str(nbFullPackets)
			print "nbTransmissions : " +str(nbTransmissions)
			print "nbTransmission Eq : " + str(nbTransmissionsCapaEq)
	
		nbNonFull = 0			
		if fullPeriodParam['leftoversSize'] > 0 :
			if lastPeriodParam['leftoversSize'] > 0 :
				nbNonFull = 2
			else : 
				nbNonFull = 1
		
		totalData = (nbTransmissionsCapaEq - nbNonFull) * self.maxPacketSize * 8 + \
				fullPeriodParam['leftoversSize'] * nbFullPeriod +  \
				lastPeriodParam['leftoversSize']  #bits

		totalTime = time / DC # ms 
		capacityMax = totalData / totalTime # bits / ms <=> kbps
		
		txTime = nbFullPackets * self.maxPacketTime() + \
					nbFullPeriod * self.dataPacketTime(fullPeriodParam['leftoversSize']) + \
					self.dataPacketTime(lastPeriodParam['leftoversSize'])
					 
		rxTime = nbTransmissions * self.ackedTransmissionRxTime() + self.APRxOvhd() * nbPeriod
		idleTime = nbTransmissions * self.ackedTransmissionIdleTime() + self.APIdleOvhd() * nbPeriod
		
		if (globConf.synchroTechCapaDebug == True) : 	
			from pprint import pprint
			print "Last Period Parameters :"
			pprint(lastPeriodParam)
		 
		 
		outputs = { 	'totalData' : totalData, \
						'totalTime' : totalTime, \
						'capacityMax' : capacityMax, \
						'txTime' : txTime, \
						'rxTime' : rxTime, \
						'idleTime' : idleTime } 
		return outputs	
			
	def energyConsumption(self, amountOfData, time, ap = None, dc = 1):
	 	"""  This function will return how many mJ we use to transmit amountOfData 
	 	within time with a given AP length / DC. 
	 	param : activePeriodLength (ms) / DC / amountOfData (bits) / time (ms)
	 	"""
		if (ap == None) :
			ap = time
					
		globalConso = 0 
		optimalInterval = (ap / dc) * pow(10,3) #us
		actualInterval = min(optimalInterval, self.synchroPeriod*pow(10,3)) #us	

		self.computeGuardInterval(actualInterval)
		
		APMax = min(ap , actualInterval * pow(10,-3) ) #ms 

		if (globConf.synchroTechConsoDebug == True) :	
			print "Actual Interval length : " + str(actualInterval)
	
	# ------------- Taking care of full time period ------------- 
		nbFullPeriod = fuzzyFloor(time*pow(10,3) / actualInterval) #FuzzyFloor ok since % after

		if (globConf.synchroTechConsoDebug == True) :
			print "Nb full period of time : " + str(nbFullPeriod)
		
		""" **************** Max Capa transmitting period conso computation : ****************
		Nb max capa tx period = data to send / maximum data we can send in one AP 
		maximum data in one AP = capacity['totalData'] (output process with AP = AP, DC = 1)
								=  capacity['numberFullPacket'] * inputs['maxPacketSize'] + capacity['leftoversSize']
		"""
				
		fullPeriodCapacity = self.capacityOutputs(APMax)
		nbMaxCapaTransmitPeriod = fuzzyFloor(amountOfData / fullPeriodCapacity['totalData']) 	
		
		maxCapaTimings = consoModeTimings( 	tx = fullPeriodCapacity['txTime'], \
											rx = fullPeriodCapacity['rxTime'], \
											idle = fullPeriodCapacity['idleTime'], \
											periodLength = actualInterval, \
											name = "Max Capa Period (us)")
		
		if (globConf.synchroTechConsoDebug == True) :
			print str(maxCapaTimings)
											  
		""" **************** Last transmitting period conso computation :  ****************	
			 How much should we still send to finish sending amoutOfData after fullTransmittingPeriod ? 
		"""
		lastFullPeriodDataToSend = amountOfData % fullPeriodCapacity['totalData']
		if (globConf.synchroTechConsoDebug == True) :
			print "Data to send in the last tx period : " + str(lastFullPeriodDataToSend)
		
		if lastFullPeriodDataToSend > 0 :
			nbTransmittingFullPeriod = nbMaxCapaTransmitPeriod + 1	
			nbFullDataPackets = fuzzyFloor(lastFullPeriodDataToSend / (self.maxPacketSize * 8)) 
			leftoverSize = lastFullPeriodDataToSend % (self.maxPacketSize * 8)
			if (globConf.synchroTechConsoDebug == True) :
				print "\t Nb Full packets : " + str(nbFullDataPackets)
				print "\t Leftovers size (bits) : " + str(leftoverSize)
			if leftoverSize > 0 :
				nbTransmission = nbFullDataPackets + 1
			else :
				nbTransmission = nbFullDataPackets 
			nbTransmissionCapaEq = nbTransmission * (1/conf.PDR)
							
			tx =  (nbFullDataPackets  * self.maxPacketTime() \
					+ self.dataPacketTime(leftoverSize)) \
				  * (1/conf.PDR)
			rx = nbTransmissionCapaEq * self.ackedTransmissionRxTime() + self.APRxOvhd() 
			idle = nbTransmissionCapaEq * self.ackedTransmissionIdleTime() + self.APIdleOvhd()
			lastTxFullPeriodTimings = consoModeTimings( tx, rx, idle,\
														periodLength = actualInterval,\
														name = "Last Transmit Full Period (us)")
								
		else : 
			nbTransmittingFullPeriod = nbMaxCapaTransmitPeriod
			lastTxFullPeriodTimings = consoModeTimings( tx = 0, rx = 0, idle = 0,\
														periodLength = 0,\
														name = "Last Transmit Full Period (us)")			
		if (globConf.synchroTechConsoDebug == True) :
			print str(lastTxFullPeriodTimings)
	
		"""  **************** Sleeping period conso computation : ****************
			during "empty period" and slave don't skip it 
		"""
		synchroTimings = self.synchroPeriodTimings(actualInterval)																		
		if nbFullPeriod > 0 :
			nbFullSleepingPeriod = nbFullPeriod - nbTransmittingFullPeriod
		else :
			nbFullSleepingPeriod = 0			
		
		if (globConf.synchroTechConsoDebug == True) :	
			print str(synchroTimings)
			print "Nb entire sleeping period " + str(nbFullSleepingPeriod)
			
	# ------------- Taking care of last non-full time period -------------		
		lastPeriodLength = (time *pow(10,3)) % actualInterval
		factor = float(lastPeriodLength) / float(actualInterval)
		"""We assume here a "full period" and make a cross multiplication 
		to get the actual value of the last period consumption """

		if (nbMaxCapaTransmitPeriod == nbFullPeriod) :
			if (globConf.synchroTechConsoDebug == True) :
				print "Last Period is used to transmit data"
			lastPeriodDataToSend = amountOfData % fullPeriodCapacity['totalData']
			nbFullDataPackets = fuzzyFloor(lastPeriodDataToSend / (self.maxPacketSize * 8))
			leftoverSize = lastFullPeriodDataToSend % (self.maxPacketSize * 8)
			if leftoverSize > 0 :
				nbTransmission = nbFullDataPackets + 1
			else :
				nbTransmission = nbFullDataPackets
			nbTransmissionCapaEq = nbTransmission * (1/conf.PDR)			
			
			tx = (nbFullDataPackets * self.maxPacketTime() \
					+ self.dataPacketTime(leftoverSize)) \
				 * (1/conf.PDR)
			rx = nbTransmissionCapaEq * self.ackedTransmissionRxTime() + self.APRxOvhd() 
			idle = nbTransmissionCapaEq * self.ackedTransmissionIdleTime() + self.APIdleOvhd()
		
		else : 
			if (globConf.synchroTechConsoDebug == True) :
				print "Last Period is a synchro period "
			tx = synchroTimings.tx
			rx = synchroTimings.rx
			idle = synchroTimings.idle
		
		lastPeriodTimings = consoModeTimings( 	tx = factor * tx,\
												rx = factor * rx,\
												idle = factor * idle ,\
												periodLength = factor * actualInterval,\
												name = "Last Period (us)")
		if (globConf.synchroTechConsoDebug == True) :	
			print str(lastPeriodTimings)

	
	#------------- Global conso computation -------------	
	
		outputs = {}

		outputs['timeTxMode'] = (nbMaxCapaTransmitPeriod * maxCapaTimings.tx \
									+ lastTxFullPeriodTimings.tx \
									+ nbFullSleepingPeriod * synchroTimings.tx \
									+ lastPeriodTimings.tx ) / pow(10,6)  #convert into s
									
		outputs['timeRxMode'] = (nbMaxCapaTransmitPeriod * maxCapaTimings.rx \
									+ lastTxFullPeriodTimings.rx \
									+ nbFullSleepingPeriod * synchroTimings.rx \
									+ lastPeriodTimings.rx ) / pow(10,6)  #convert into s
									
		outputs['timeIdleMode'] = (nbMaxCapaTransmitPeriod * maxCapaTimings.idle \
									+ lastTxFullPeriodTimings.idle \
									+ nbFullSleepingPeriod * synchroTimings.idle \
									+ lastPeriodTimings.idle ) / pow(10,6)  #convert into s
									
		outputs['timeSleepMode'] = (nbMaxCapaTransmitPeriod * maxCapaTimings.sleep \
									+ lastTxFullPeriodTimings.sleep \
									+ nbFullSleepingPeriod * synchroTimings.sleep \
									+ lastPeriodTimings.sleep ) / pow(10,6)  #convert into s
									
# 		outputs['timeSleepCalc'] = time * pow(10,-3) - \
# 						(outputs['timeTxMode'] + outputs['timeRxMode'] + outputs['timeIdleMode'])

		#All timings are now in s
		globalTimings = consoModeTimings(	tx = outputs['timeTxMode'],\
											rx = outputs['timeRxMode'] ,\
											idle = outputs['timeIdleMode']  ,\
											periodLength = time * pow(10,-3),\
											name = "Global Timings (s)")
		if (globConf.synchroTechConsoDebug == True) :	
			print str(globalTimings)
			print self.hardware
		#s * mW
		globalConso = globalTimings.tx * self.hardware.txConso \
					+ globalTimings.rx * self.hardware.rxConso \
					+ globalTimings.idle * self.hardware.cpuOnConso \
					+ globalTimings.sleep * self.hardware.sleepConso
	
	
		outputs['energyConsumption'] = globalConso # mJ
		if (globConf.synchroTechConsoDebug == True) :	
			print "Consumption tx : " + str(globalTimings.tx * self.hardware.txConso)  + 'mJ'
			print "Consumption rx : " + str(globalTimings.rx * self.hardware.rxConso)  + 'mJ'
			print "Consumption idle : " + str(globalTimings.idle * self.hardware.cpuOnConso)  + 'mJ'
			print "Consumption sleep : " + str(globalTimings.sleep * self.hardware.sleepConso)  + 'mJ'
# 	
# 		outputs['Iavg'] = (outputs['timeTxMode'] * conso['pTx'] + outputs['timeRxMode'] * conso['pRx'] +\
# 							 outputs['timeIdleMode'] * conso['pIdle'] + outputs['timeSleepMode'] * conso['pSleep'])\
# 							 / (constraints['time']  * pow(10,-3) * conso['voltage'])
	
		self.resetGuardInterval()
		 
		return outputs
	
	def __repr__(self):
		myStr = Techno.__repr__(self)
		myStr += 'Synchro period : ' + str(self.synchroPeriod) + ' ms\n'
		return myStr
		
	#Abstract methods		
	def synchroPeriodTimings(self, period):
		raise NotImplementedError	
			
	def APRxOvhd(self):
		raise NotImplementedError
		
	def APIdleOvhd(self):
		raise NotImplementedError
		