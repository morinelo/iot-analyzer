from Hardware import Hardware
from fuzzyFloor import *
import globConf

class Techno (object): 
	""" Class defining a technology for the global consumption comparison """
	
	def __init__(self, name, maxPacketSize, PHYRate, PHYOvhd, MACOvhd, SixOvhd,\
					clockDrift, platform_name = "greennet") :

		#Common attributes to all protocols
		self.name = name
		self.maxPacketSize = maxPacketSize #bytes
		self.PHYRate = PHYRate #Mbps
		self.PHYOvhd = PHYOvhd #us
		self.MACOvhd = MACOvhd #bits
		self.SixOvhd = SixOvhd #bits
		
		# Guard interval :
		# We only set this value whithin function that needs it.
		self.clockDrift = clockDrift
		self.guardInterval = 0 
		
		#Hardware choice for this techno
		self.setHardware(platform_name)
			
	def totalOvhd(self):
		return  (self.SixOvhd + self.MACOvhd) / self.PHYRate  + self.PHYOvhd

	def ackedTransmissionOvhd(self):
		#return time in us
		return self.ackedTransmissionRxTime() + self.ackedTransmissionIdleTime()
		
	def dataPacketTime(self, size):
	 	"""This function compute the time on air for a given packet size in bits (taking
	 	into account 6Ovh / MACOVh and PHYOvhd), return time in us
	 	"""
		if ( size != 0 and size <= self.maxPacketSize * 8) :
			return size / self.PHYRate + self.totalOvhd()
		elif size == 0 :
			return 0
		else : 
			raise ValueError('The data packet you try to transmit is to big !')

	def ackTransmissionTime(self,datasize): 
		return self.dataPacketTime(datasize) + self.ackedTransmissionOvhd()
		
	def maxPacketTime(self):
		return self.dataPacketTime(self.maxPacketSize * 8)
	
	def computeMaxPacketCost(self):
		""" This function compute the maxPacket Time and the corresponding AP
		length. It allows to estimate a retransmission cost of a full data 
		packet.
		"""
		time = self.maxPacketTime()
		transmissionTime = time + self.ackedTransmissionOvhd()
		length = self.computeAPLengthOnePacket(time)
		return	{'time' : time , 'APlength' : length, 'ackedTxTime' : transmissionTime }
		
	
	def setHardware(self, hw_name):
		availableHW = self.getAvailableHWList()
		for hw in availableHW :
			if (hw.name == hw_name):
				self.hardware = hw
				break
		else :
			raise UserWarning(self.name + " HW not existing yet")

	def __repr__(self):
		myStr = "----- Common values of "+str(self.name)+ " -----\n"
		myStr += str(self.hardware) + "\n"
		myStr += 'Maximum Packet Size : ' + str(self.maxPacketSize) + ' bytes\n'
		myStr += 'Available PHY Rate : ' + str(self.PHYRate) + ' Mbps\n'
		myStr += 'PHY Overhead : ' + str(self.PHYOvhd) + ' us\n'
		myStr += 'MAC Overhead : ' + str(self.MACOvhd) + ' bits\n'
		myStr += 'Ipv6 Overhead : ' + str(self.SixOvhd) + ' bits\n'
		return myStr
	
	def optimizedEnergy(self, data,time):
		"""This function depends on your choice of optimization. Take data in bits and time in ms"""
		capacity = self.capacityOutputs(time) 
	
		energy = float('inf')

		from math import floor	#Issue with python :D
		if ( floor(data) > floor(capacity['totalData'])) :
			if (globConf.capaDebug == True) :
				print "/!\ "+str(self.name)+" does not support " \
						+ str(data*pow(10,-3)) +  ' kbits/' + str(time) + 'ms'
		else :			
			energyComputationOutputs = self.energyConsumption(floor(data),time)
			energy = energyComputationOutputs['energyConsumption']
		
		return energy
		
	# ***************************  Abstract methods	*************************** 
	
	def getAvailableHWList(self):
		raise NotImplementedError
	
	def ackedTransmissionIdleTime(self):
		raise NotImplementedError
	
	def ackedTransmissionRxTime(self):
		raise NotImplementedError

	def capacityOutputs(self, time, DC = 1):
		raise NotImplementedError
	 	
	def energyConsumption(self, amountOfData, time, ap = None, dc = 1):
		""" This function compute the energy consumption with given parameters
		data in bits, time in ms
	 	"""
	 	raise NotImplementedError