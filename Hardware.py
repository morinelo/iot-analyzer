class Hardware(object): #Class a generic Harware	
	"""Class defining a generic hardware, caracterized by its consumption in different mode : rx / tx / sleep  (mW) and its name:
		- rxConso
		- txConso
		- sleepConso
		- name """	
 
	def __init__(self, dict = None):
		"""This function creates a default hw based on GN parameters,
		 but you can precise all param within a dictionnary of :
		 { 	'name' : name, 
		 	'rx' : rx_value, 
		 	'tx' : tx_value, 
		 	'cpu' : cpu_on,
		 	'sleep' : sleep_value }"""
		if (dict == None) :
			self.name = 'default (GN)'
			self.rxConso = 19.26 #mW
			self.txConso = 24.38 #mW	
			self.cpuOnConso = 10 #mW
			self.sleepConso = 5.76*pow(10,-3) #mW
		else : 
			try :
				self.name = dict['name']
				self.rxConso = dict['rx']
				self.txConso = dict['tx']
				self.cpuOnConso = dict['cpu']
				self.sleepConso = dict['sleep']
			except KeyError as e :		
				raise UserWarning ("Hardware could not initialized properly : missing key "+str(e))
	
	def __repr__(self):
		string = "Hardware \'" + str(self.name) 
		string += "\' consumes : \n" 
		string += "\t" + str(self.txConso) + "mW in Tx \n"
		string += "\t" + str(self.rxConso) + "mW in Rx \n"
		string += "\t" + str(self.cpuOnConso) + "mW for cpu On \n"
		string += "\t" + str(self.sleepConso) + "mW in sleep mode"
		return string

#****************************************** Tests ******************************************		
if __name__ == '__main__' : 

	print "\t\t\t\t\t\t* ------------------- *"
	print "\t\t\t\t\t\t Testing hardware class "
	print "\t\t\t\t\t\t* ------------------- *\n"
	myHW = Hardware() #This will generate the default GN values 
	print myHW
	
	mydictHW = Hardware(dict = { \
							'name' : 'test',\
						 	'rx' : 1,\
						 	'tx' : 2,\
						 	'cpu' : 3,\
						 	'sleep' : 0.01 \
						} )
	print mydictHW
	
	mydictErrorHW = Hardware(dict = { \
							'name' : 'test',\
						 	'tx' : 2,\
						 	'cpu' : 3,\
						 	'sleep' : 0.01 \
						} )