from SynchroTechno import SynchroTechno
from PSMHardware import *

class PSM(SynchroTechno):
	"""This class aims at modeling 802.15.4 functionning"""
	def __init__(self, hwname, version, clockDrift = 'inf'):
		#Hwname should be in implemented PSMHarware List
		#Version is either 5.5 or 11	
		
		#--Management MAC format - beacon (bytes)
		fc = 2 
		duration = 2
		receiverAddress = 6
		senderAddress = 6 
		BSSID = 6
		seqCtrl = 2
		FCS = 4
		#-- Beacon MAC body
		timestamp = 8 #7.3.1.10
		BI = 2 #7.3.1.3
		capability = 2 #7.3.1.4
		#--Beacon information Element (7.3 ...)
		IEheader = 2 # Id, length
		SSID = 0 # 0 (broadcast) to 32
		Rates = 1 # 1 to 8 
		#(each octet describe a single supported rate in unit of 500kbps)
		DSParameterSet = 1 #current channel (figure 38)
		TIM = 4 # DTIM Count (1), DTIM Period (1),
				# Bitmap ctrl (1), Partial virtual bitmap (1-251)
		IEBeacons = (IEheader + SSID) \
					+ (IEheader + Rates) \
					+ (IEheader + DSParameterSet) \
					+ (IEheader + TIM)	
		
		beaconSize = fc + duration \
					+ receiverAddress + senderAddress \
					+ BSSID + seqCtrl + FCS + timestamp \
					+ BI + capability + IEBeacons
	
		
		if version == '5.5' :
			rate = 5.5
			phyovhd = 120 / rate #120 bits short Preamble  :
			# (PLCP header + preamble)
			SynchroTechno.__init__(	self,\
								name = "PSM5.5",\
								platform_name = hwname,\
								maxPacketSize = 1280,\
								PHYRate = rate,\
								PHYOvhd = phyovhd,\
								MACOvhd = 272,\
								SixOvhd = 320,\
								synchroPeriod = 1024 * (pow(2,16) - 1),\
								CD = clockDrift)		
										
		elif (version == '11') :
			rate = 11.0
			phyovhd = 120 / rate #120 bits short Preamble  :
			# (PLCP header + preamble)
			
			SynchroTechno.__init__(	self,\
								name = "PSM11",\
								platform_name = hwname,\
								maxPacketSize = 1280,\
								PHYRate = rate,\
								PHYOvhd = phyovhd,\
								MACOvhd = 272,\
								SixOvhd = 320,\
								synchroPeriod = 1024 * (pow(2,16) - 1),\
								CD = clockDrift)	
					
		else :
			raise UserWarning("PSM Version not implemented")
			
		self.version = version
		self.SIFS = 10.0 # us
		aSlotTime = 20.0 # us 
		self.DIFS = self.SIFS + 2 * aSlotTime #us
		self.backoff = 0 # us 			
		beaconMACFrameDuration = (beaconSize * 8) / self.PHYRate # us 
		self.beacon = beaconMACFrameDuration + self.PHYOvhd

		self.ack = (14 * 8) / self.PHYRate 
		#us (14 bytes, figure 18 802.11-1999)	
	
	def getAvailableHWList(self):
		from PSMHardware import PSMHardware
		return PSMHardware.getList()	
		
	def ackedTransmissionRxTime(self):
		return (self.ack + (self.DIFS + self.backoff))
		
	def ackedTransmissionIdleTime(self):
		return self.SIFS
	
	def APRxOvhd(self):
		return self.guardInterval + self.beacon
	
	def APIdleOvhd(self):
		return 0
	
	def synchroPeriodTimings(self, period):
		from consoModeTimings import consoModeTimings
		synchroTimings = consoModeTimings( 	tx = 0,\
											rx = self.APRxOvhd(),\
											idle=self.APIdleOvhd(),\
											periodLength = period,\
											name = "Synchro Period ")
		return synchroTimings
	
	def __repr__(self):
		myStr =	SynchroTechno.__repr__(self)
		myStr += "----- Power Saving Mode specific values -----\n"
		myStr += "Version : " + str(self.version) + '\n'
		myStr += 'SIFS : ' + str(self.SIFS) + ' us\n'
		myStr += 'DIFS : ' + str(self.DIFS) + ' us\n'
		myStr += 'Backoff : ' + str(self.backoff) + ' us\n'
		myStr += 'Ack Duration: ' + str(self.ack) + ' us\n'
		myStr += 'Beacon Duration : ' + str(self.beacon) + ' us\n'
		return myStr
		
				
#****************************************** Tests ******************************************		
if __name__ == '__main__' : 
	from pprint import pprint	
	myPSM5 = PSM('MAX2830','5.5')
	print myPSM5
	
	myPSM11 = PSM('MAX2830','11')
	print myPSM11 
	
	print "\n *----- PSM Max Packet Cost Test -----*"
	print "--> PSM 5.5 : " + str(myPSM5.computeMaxPacketCost())
	print "--> PSM 11 : " + str(myPSM11.computeMaxPacketCost())
	
	print "\n *----- PSM Capacity output test -----*"
	print "Computing maximum data to send within 500 ms\n"
	print "--> PSM 5.5 (Expected totalData : 2 470 180 bits) : "
	pprint(myPSM5.capacityOutputs(time = 500))
	""" Expectation explanation : 
	Within 500 ms : 
	One non full transitting period
	(500 000 us - beacon) 
	= 499 899.63 us to transmit data
	241 full packet + 637 us
	637 us - transmissionOvhd = 556 us
	556 us - pcktOvhd = 426 us 
	--> leftover approx. 2340 bits (292 bytes)
	so, total = 241 * 1280 * 8 + 2340 
	total = 2 470 180 bits
	"""
	print "\n--> PSM 11 (Expected totalData : 4 802 560 bits) : "
	pprint(myPSM11.capacityOutputs(time = 500))	
	""" Expectation explanation : 
	Within 500 ms : 
	One non full transitting period
	(500 000 us - beacon) 
	= 499 949.82 us to transmit data
	469 full packets + 81 us
	81 us - transmissionOvhd < 0 
	So, total = 469 * 1280 * 8
	total = 4 802 560 bits
	"""


	print "\n *----- PSM energy consumption test -----*"
	print "Computing energy consumption (mJ) to handle 100 bytes per 100s\n"
	print "--> PSM 5.5 : "
	#Be careful, commented results are not taking into account GI (
	#(disable it to check)
	pprint(myPSM5.energyConsumption(amountOfData = 800, time = 100000))
	""" Expectation : 
	Tx Time = 1T(100bytes) = 275 us
	Rx Time = Tbeacon + Tdifs + backoff + Tack 
		 = 100.3636 + 50 + 0 + 20.3636 = 170.7272 us
	Idle Time = Tsifs = 10 us
	"""
	print "\n--> PSM 11 : "
	pprint(myPSM11.energyConsumption(amountOfData = 800, time = 100000))
	""" Expectation
	Tx Time = 1T(100bytes) = 137 us
	Rx Time = Tbeacon + Tdifs + backoff + Tack 
		 = 50.1818 + 50 + 0 + 10.1818 = 110.3636 us
	Idle Time = Tsifs = 10 us
	"""