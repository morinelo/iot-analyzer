from SynchroTechno import SynchroTechno

class ah(SynchroTechno):
	"""This class aims at modeling 802.11ah functionning"""
	def __init__(self, hwname, phyRate, tech_name , clockDrift):
		#Hwname should be in implemented ahHardware List
		#Version is in [0_1, 2_1, 10_1, 8_2, 3_4, 5_4, 9_16]
		bigPHYOvhd = 560.0 #us
		shortPHYOvhd = 240.0 #us
		syncPeriod = pow(10,4)* (pow(2,14)-1) * pow(10,3) #(ms) 
		
		if tech_name in ["0_1", "2_1", "10_1"]:
			phyOvhd = bigPHYOvhd
		elif tech_name in ["8_2", "3_4", "5_4", "9_16"]:	
			phyOvhd = shortPHYOvhd
		else :
			raise UserWarning("ah variant not implemented yet")
			
		SynchroTechno.__init__(	self,\
					name = tech_name,\
					platform_name = hwname,\
					maxPacketSize = 1280.0,\
					PHYRate = phyRate,\
					PHYOvhd = phyOvhd,\
					MACOvhd = 80.0,\
					SixOvhd = 320.0 ,\
					synchroPeriod = syncPeriod,
					CD = clockDrift)	
						
		self.version = tech_name
		self.SIFS = 160.0 #us
		self.DIFS= 264 #us 
		self.backoff= 0 #us
		
		#unit = bytes (cf ah survey : 4.2.3. Short beacons)
		fc = 2 
		duration = 2
		senderAddress = 6 
		FCS = 4
		timestamp = 4 
		BI = 2 
		capability = 2 
		IEheader = 2 # Id, length
		SSID = 0 # 0 (broadcast) to 32
		Rates = 1 # 1 to 8 (each octet describe a single supported rate in unit of 500kbps)	   
		DSParameterSet = 1 #current channel (figure 38)
		TIM = 4 # DTIM Count (1), DTIM Period (1), Bitmap ctrl (1), Partial virtual bitmap (1-251)
		IEBeacons = (IEheader + SSID) +\
					(IEheader + Rates) +\
					(IEheader + DSParameterSet) +\
					(IEheader + TIM)
		beaconMACFrameSize = fc + duration + senderAddress + FCS + timestamp + BI + capability + IEBeacons
		
		# (bits / Mbps -> us) 
		self.beaconTime = (beaconMACFrameSize * 8) / self.PHYRate + self.PHYOvhd
		self.ackTime = self.PHYOvhd
	
	def getAvailableHWList(self):
		from ahHardware import ahHardware
		return ahHardware.getList()	
		
	def ackedTransmissionRxTime(self):
		return (self.ackTime + (self.DIFS + self.backoff))
		
	def ackedTransmissionIdleTime(self):
		return self.SIFS
	
	def APRxOvhd(self):
		return self.guardInterval + self.beaconTime
	
	def APIdleOvhd(self):
		return 0
	
	def synchroPeriodTimings(self, period):
		from consoModeTimings import consoModeTimings
		synchroTimings = consoModeTimings( 	tx = 0,\
											rx = self.APRxOvhd(),\
											idle=self.APIdleOvhd(),\
											periodLength = period,\
											name = "Synchro Period ")
		return synchroTimings
																
	def __repr__(self):
		myStr =	SynchroTechno.__repr__(self)
		myStr += "----- 802.11ah specific values -----\n"
		myStr += 'Version : ' + str(self.version) + '\n'	
		myStr += 'SIFS : ' + str(self.SIFS) + ' us\n'
		myStr += 'DIFS : ' + str(self.DIFS) + ' us\n'
		myStr += 'Backoff : ' + str(self.backoff) + ' us\n'
		myStr += 'Ack Duration: ' + str(self.ackTime) + ' us\n'
		myStr += 'Beacon Duration : ' + str(self.beaconTime) + ' us\n'
		return myStr
		
class ah0_1(ah):
	def __init__(self, hwname, CD = 'inf'):
		ah.__init__(self, hwname, clockDrift = CD,\
					 phyRate = 0.3, tech_name = "0_1")
					 
class ah2_1(ah):
	def __init__(self, hwname, CD = 'inf'):
		ah.__init__(self, hwname, clockDrift = CD,\
					 phyRate = 0.9, tech_name = "2_1")
	
class ah10_1(ah):
	def __init__(self, hwname, CD = 'inf'):
		ah.__init__(self, hwname, clockDrift = CD,\
					 phyRate = 0.15, tech_name = "10_1")	
					 
class ah8_2(ah):
	def __init__(self, hwname, CD = 'inf'):
		ah.__init__(self, hwname, clockDrift = CD,\
					 phyRate = 7.8, tech_name = "8_2")
					 
class ah3_4(ah):
	def __init__(self, hwname, CD = 'inf'):
		ah.__init__(self, hwname, clockDrift = CD ,\
					 phyRate = 5.4, tech_name = "3_4")
					 
class ah5_4(ah):
	def __init__(self, hwname, CD = 'inf'):
		ah.__init__(self, hwname, clockDrift = CD ,\
					 phyRate = 10.8, tech_name = "5_4")
					 
class ah9_16(ah):
	def __init__(self, hwname, CD = 'inf'):
		ah.__init__(self, hwname, clockDrift = CD,\
					 phyRate = 78.0, tech_name = "9_16")
					 
					 
#********************* Tests **********************		
if __name__ == '__main__' : 
	from pprint import pprint		
	myZeroOne = ah0_1('MasterThesis')
	print myZeroOne

	myTwoOne = ah2_1('oldPaper20Rx')
	print myTwoOne
	
	myTenOne = ah10_1('oldPaper100Rx')
	print myTenOne
	
	myEightTwo = ah8_2('oldPaper200Rx')
	print myEightTwo
	
	myThreeFour = ah3_4('G2M5477')	
	print myThreeFour
	
	myFiveFour = ah5_4('oldPaper20Rx')
	print myFiveFour
	
	myNineSixteen = ah9_16('oldPaper20Rx')
	print myNineSixteen
	
	print "\n *----- ah Max Packet Cost Test -----*"
	print "--> ah 10_1 : " + str(myTenOne.computeMaxPacketCost())
	print "--> ah 8_2 : " + str(myEightTwo.computeMaxPacketCost())
	print "--> ah 9_16 : " + str(myNineSixteen.computeMaxPacketCost())

	#Test needed for Energy Consumption and Capacity Outputs

	