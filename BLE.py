from SynchroTechno import SynchroTechno

class BLE(SynchroTechno):
	"""This class aims at modeling BLE functionning"""
	def __init__(self, hwname, version, clockDrift = 'inf', phyRate5 = None):
		#Hwname should be in implemented BLEHarware List
		#Version is either 4.0 or 4.2
		LLDataHeader = 32		
		macOvhd = 16 +  LLDataHeader #No MIC  (bits)

		if version == '4.0' :
			SynchroTechno.__init__(	self,\
								name = "BLE4.0",\
								platform_name = hwname,\
								maxPacketSize = 21,\
								PHYRate = 1,\
								PHYOvhd = 64,\
								MACOvhd = macOvhd,\
								SixOvhd = 16,\
								synchroPeriod = 32000,
								CD = clockDrift)		
		elif (version == '4.2') :
			self.__init__(hwname, '4.0', clockDrift)
			self.name = "BLE4.2"
			self.maxPacketSize = 245
		elif (version == '5.0') :
			self.__init__(hwname, '4.0', clockDrift)
			self.name = "BLE5.0"
			self.maxPacketSize = 245
			if (phyRate5 == None) :
				self.PHYRate = 2
				self.PHYOvhd = 36 #us
			else :
				self.PHYRate = phyRate5
				if (phyRate5 == 0.125) :
					self.PHYOvhd = 592 #us
				elif (phyRate5 == 0.500) :
					self.PHYOvhd = 430 #us
				elif (phyRate5 == 1 ) :
					self.PHYOvhd = 64 #us
				else :
					raise UserWarning("Impossible BLE 5.0 BitRate" + str(phyRate5))
		else :
			raise UserWarning("BLE Version not implemented")
		
		self.version = version
		self.TIFS = 150 #us
		#No data packet
		self.ackTime = (self.MACOvhd - LLDataHeader) / 	self.PHYRate \
							+ self.PHYOvhd	#us			
		#Empty packet
		self.pollTime = self.MACOvhd / self.PHYRate + self.PHYOvhd #us		
	
	def getAvailableHWList(self):
		from BLEHardware import BLEHardware
		return BLEHardware.getList()	

	def ackedTransmissionRxTime(self):
		return self.ackTime
		
	def ackedTransmissionIdleTime(self):
		return 2 * self.TIFS
	
	def APRxOvhd(self):
		return self.guardInterval + self.pollTime
	
	def APIdleOvhd(self):
		return self.TIFS
	
	def synchroPeriodTimings(self, period):
		from consoModeTimings import consoModeTimings
		synchroTimings = consoModeTimings( 	tx = self.ackTime,\
											rx = self.APRxOvhd(),\
											idle=self.APIdleOvhd() + self.TIFS,\
											periodLength = period,\
											name = "Synchro Period ")
		return synchroTimings
																
	def __repr__(self):
		myStr =	SynchroTechno.__repr__(self)
		myStr += "----- BLE specific values -----\n"
		myStr += 'Version : ' + str(self.version) + '\n'	
		myStr += 'TIFS : ' + str(self.TIFS) + ' us\n'
		myStr += 'Ack Duration: ' + str(self.ackTime) + ' us\n'
		myStr += 'Poll Duration : ' + str(self.pollTime) + ' us\n'
		return myStr
		
				
#****************************************** Tests ******************************************		
if __name__ == '__main__' : 
	from pprint import pprint		
	myBLE = BLE('nRF51822','4.0')
	print myBLE
	
	myBLETwo = BLE('nRF51822','4.2')
	print myBLETwo 

	myBLEFive = BLE('nRF51822','5.0')
	print myBLEFive 
	
	print "\n *----- BLE Max Packet Cost Test -----*"
	print "--> BLE 4.0 : " + str(myBLE.computeMaxPacketCost())
	print "--> BLE 4.2 : " + str(myBLETwo.computeMaxPacketCost())
	print "--> BLE 5.0 : " + str(myBLEFive.computeMaxPacketCost())
	
	print "\n *----- BLE Capacity output test -----*"
	print "Computing maximum data to send within 40 s\n"
	print "--> BLE 4.0 (Expected totalData : 9940608) : "
	pprint(myBLE.capacityOutputs(time = 40000))
	print "\n--> BLE 4.2 (Expected totalData : 31765812) : "
	pprint(myBLETwo.capacityOutputs(time = 40000))
	
	print "\n *----- BLE energy consumption test -----*"
	print "Computing energy consumption (mJ) to handle 100 bytes per 100s\n"
	print "--> BLE 4.0 : "
	#Be careful, commented results are not taking into account GI (disable it to check)
	pprint(myBLE.energyConsumption(amountOfData = 800, time = 100000))
	#Tx Time = 4TdataMax + 1T(16bytes) + 3Tack = 1610 us
	#Rx Time = 4*Tpoll + 5*Tack + (1/8 * Tpoll) = 750 us
	#Idle Time = (11+2+2+2) TIFS = 2287.5 us
	print "\n--> BLE 4.2 : "
	pprint(myBLETwo.energyConsumption(amountOfData = 800, time = 100000))
	#Tx Time = T(100bytes) + 3Tack = 1098 us
	#Rx Time = 4*Tpoll + 1*Tack = 430 us
	#Idle Time = (3+2+2+2) TIFS = 1087.5 us 

	