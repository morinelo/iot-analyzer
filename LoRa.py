from Techno import Techno
import conf
import globConf
from math import ceil
from fuzzyFloor import fuzzyFloor,fAlmostEqual
from consoModeTimings import consoModeTimings

class LoRa(Techno):
	
	def __init__(self, dc, hwname, version, drift = 'inf') :
		#No IPv6 for now, PHYRateUL = PHYRateDL (here)
	
		if (version == "EuMin") : 
			BW = 125.0*pow(10,-3) #MHz
			self.SF = 12.0
			maxPacket = 59.0 # bytes
		elif (version == "EuMax") : 
			BW = 250.0*pow(10,-3) #MHz
			self.SF = 7.0
			maxPacket = 250.0 # bytes
		else : 
			raise UserWarning("LoRa "+ str(version) + " is not implemented")
		
		self.version = version
		self.CR = 4.0/6.0 # TODO = check the coding rate
		self.DC = dc #Time percentage on one freq (ETSI)
		self.nbAvailableSubbands = 1 #For now, no-one knows (-> 1 only)
		self.Tsymb = pow(2, self.SF) / BW
		#rate = self.SF * 1/ ((4.0+self.CR)*self.Tsymb)
		rate = self.SF * self.CR / self.Tsymb
		
		Techno.__init__(self, name = "LoRa"+str(version)+str(dc), \
							maxPacketSize = maxPacket, \
							PHYRate = rate, \
							PHYOvhd = "N/A", \
							MACOvhd = "N/A", \
							SixOvhd = 0,\
							clockDrift = drift,\
							platform_name = hwname)
							
		self.preambleLen = 8
		
		#MAC and FHDR format (size in bytes)
		MHDR = 1 
		MIC = 4
		DeviceAddr = 4
		FCtrl = 1
		FCnt = 2
		FOpt = 0 # Up to 15 
		FHDR = DeviceAddr + FCtrl + FCnt + FOpt
		FportData = 1 
		self.MACOvhdData = (MHDR + (FHDR + FportData) + MIC) #bytes
		FportAck = 0
		
		self.MACOvhdEmptyPacket = (MHDR + (FHDR + FportAck) + MIC) #bytes
		
		interFrameTime = 0#us
		self.RECEIVE_DELAY1 = 1 * pow(10,3) #ms (1s)
		
	def ackTime(self):	
		return self.dataPacketTime(0) #in ms
			
	def getAvailableHWList(self):
		from LoRaHardware import LoRaHardware
		return LoRaHardware.getList()	

	def dataPacketTime(self, size): 
	 	"""This function compute the time on air 
	 	for a given packet size in bits 
	 	( /!\ LoRa formula needs size in bytes)
	 	return time in ms
	 	"""	
	 	sizeBytes = fuzzyFloor(size / 8)
	 	if (sizeBytes > 0) and (sizeBytes <= self.maxPacketSize) :
	 		MacPL = sizeBytes + self.MACOvhdData
	 	elif (sizeBytes == 0) : 
	 		MacPL = sizeBytes + self.MACOvhdEmptyPacket
		else : 
			raise ValueError('The data packet you try to transmit is to big !')
	 	
	 	NbSymbPreamble = self.preambleLen + 4.25
	 	IH = 0 # Implicit header (=0 header on / =1 no header)
		DE = 0 #Low Data Rate optimization (0 =off / 1 =on)
	
		#Compute LoRa Formulation
		numerator = 8.0*MacPL - 4*self.SF +28+16-20*IH
		denom = 4.0* (self.SF - 2*DE)
		NbSymbPL = 8 + max( ceil(numerator/denom)*(self.CR+4) ,0)
		time = (NbSymbPreamble + NbSymbPL) * self.Tsymb
		return time * pow(10,-3) #ms
	
	def ackedTransmissionIdleTime(self):
		return self.RECEIVE_DELAY1 # ms
		
	def ackedTransmissionRxTime(self):
		return self.ackTime() # ms
		
	def capacityOutputs(self, time):
		#Full packet
		timeOnPerSubband = time * (self.DC/100.0) # ms
		if (globConf.LoRaDebug == True) : 
			print "<---- Capacity computation during " + str(time) + " ms"
			print "ETSI Limitation Ton : " + str(timeOnPerSubband) + " ms" 
		nbFullTxPerSubband = fuzzyFloor(timeOnPerSubband / self.maxPacketTime()) 
		if (globConf.LoRaDebug == True) : 
			print "Nb Full Packet : " + str(nbFullTxPerSubband)
		#Last packet
		deltatime = timeOnPerSubband % self.maxPacketTime() #ms
		#++++ Error with floating point
		if fAlmostEqual(deltatime,0) or (deltatime <0) :
			deltatime = 0	
		#++++ Determines number of bits last Tx			
		timeLast = 0 
		lastPacketSize = -1	#To have correct lastPacketSize ending value 
		while timeLast < deltatime :	
			timeLast = self.dataPacketTime(lastPacketSize + 1) #ms
			lastPacketSize += 1
		
		#Number of transmissions
		nbTransmissions = nbFullTxPerSubband		
		if lastPacketSize > 0 :
			nbTransmissions += 1 #Counting one more transmission
					
		if (globConf.LoRaDebug == True) : 	
			print "Last Packet Size : " + str(lastPacketSize) + " bits"
			print "Last Packet Time : " + str(timeLast) + " ms"
		
		#Sum-up
		nbTransmissionsCapaEq =	nbTransmissions / (1/conf.PDR)
		maxDataPerSubband= nbTransmissionsCapaEq  * self.maxPacketSize * 8 + lastPacketSize		
				
		#Maximal capacity is limited not only by available Subband, 
		# but also by the time :
		# even if 16 av. subbands, if DC is 1/10,
		# > 10 channels will not be helpful : 
		# We can reuse the first channel after 9 others
		nbUsefulSubband = min(self.nbAvailableSubbands, fuzzyFloor(100.0/self.DC)) 
		maxDataETSI =  maxDataPerSubband * nbUsefulSubband # bytes	
		
		txTime = self.maxPacketTime() * nbFullTxPerSubband + timeLast \
					* nbUsefulSubband 
		idleTime = self.ackedTransmissionIdleTime() * nbTransmissions
		rxTime = self.ackedTransmissionRxTime() * nbTransmissions
	
		#Handling capacity limitation by time
		if (maxDataETSI > 0) : 
			timeAllowedOvhd = time - txTime
	
			maxETSIOvhdTime = self.ackedTransmissionOvhd() * nbTransmissions \
								*  nbUsefulSubband #ms
								
			if (maxETSIOvhdTime > timeAllowedOvhd) :
				print "--$$$$-- ERROR ERROR --$$$$--" 
				raise UserWarning("ETSI limitation is less important than Ovhd needed time")	
	
		if (globConf.LoRaDebug == True) : 
			print "Max data : " + str(maxDataETSI)+ 'bits'

		# bits / ms <=> kbps 
		capacityMax = maxDataETSI / time
		
		outputs = { 	'totalData' : maxDataETSI, \
						'totalTime' : time, \
						'capacityMax' : capacityMax, \
	 					'txTime' : txTime, \
						'rxTime' : rxTime, \
						'idleTime' : idleTime \
					} 
		if (globConf.LoRaDebug == True) : 
			from pprint import pprint
			pprint(outputs)
			print "---- End capacity ---->"
		return outputs	
			
	def energyConsumption(self, amountOfData, time, ap = None, dc = 1):
		# This function assume amountOfData over time respect capacity constraints 
		# Return : nb of mJ used to transmit amountOfData (bits) within time (ms)

		nbFullPacketsNeeded = fuzzyFloor(float(amountOfData / 8) / self.maxPacketSize) #fuzzyFloor ok, % after
		lastPacketSize = float(amountOfData / 8) % self.maxPacketSize

		if (globConf.LoRaDebug == True) :	
			print "<----- Energy computation -----"
			myStr = "To send " + str(amountOfData) + "bits : " 
			myStr += str(nbFullPacketsNeeded) + " full packets + "
			myStr += str(lastPacketSize) + " bytes"
			print myStr

		# --- Full data packet transmissions
		nbFullTransmission = nbFullPacketsNeeded  * (1/conf.PDR)
		txTime = self.maxPacketTime() * nbFullTransmission
		idleTime = self.ackedTransmissionIdleTime() * nbFullTransmission
		rxTime  = self.ackedTransmissionRxTime() * nbFullTransmission
	
		if (globConf.LoRaDebug == True) :	
			print "Full Pckts :: "
			print "TDataMax " + str(txTime) + " ms"
			print "totalRxTime " + str(rxTime) + " ms"

		# --- Last packet transmissions
		if lastPacketSize != 0 : 
			txTime += self.dataPacketTime(lastPacketSize * 8) * (1/conf.PDR)
			idleTime += self.ackedTransmissionIdleTime() * (1/conf.PDR)
			rxTime += self.ackedTransmissionRxTime() * (1/conf.PDR)
		else : 
			pass #No need of an non full data packet

		#Convert us to s 
		outputs = {}
		outputs['timeTxMode'] = txTime * pow(10,-3)
		outputs['timeRxMode'] = rxTime * pow(10,-3)
		outputs['timeIdleMode'] = idleTime * pow(10,-3)
				
		#All timings are now in s
		globalTimings = consoModeTimings(	tx = outputs['timeTxMode'],\
											rx = outputs['timeRxMode'] ,\
											idle = outputs['timeIdleMode'] ,\
											periodLength = time * pow(10,-3),\
											name = "LoRa Global Timings (s)")
		outputs['timeSleepMode'] = globalTimings.sleep
		
		#s * mW
		globalConso = globalTimings.tx * self.hardware.txConso \
					+ globalTimings.rx * self.hardware.rxConso \
					+ globalTimings.idle * self.hardware.cpuOnConso \
					+ globalTimings.sleep * self.hardware.sleepConso

		outputs['energyConsumption'] = globalConso # mJ

		if (globConf.LoRaDebug == True) :	
			print str(globalTimings)
			print self.hardware
			print "Consumption tx : " + str(globalTimings.tx * self.hardware.txConso)  + 'mJ'
			print "Consumption rx : " + str(globalTimings.rx * self.hardware.rxConso)  + 'mJ'
			print "Consumption idle : " + str(globalTimings.idle * self.hardware.cpuOnConso)  + 'mJ'
			print "Consumption sleep : " + str(globalTimings.sleep * self.hardware.sleepConso)  + 'mJ'
			print "Global conso : " + str(outputs['energyConsumption']) + " mJ"
			print "---- End energy --->"
			
		return outputs
	
	def __repr__(self):
		myStr = Techno.__repr__(self)
		myStr += "----- LoRa "+ self.version + "specific values -----\n"
		myStr += 'Spreading Factor : ' + str(self.SF) + '\n'
		myStr += 'Coding Rate : ' + str(self.CR) + '\n'
		myStr += 'Duty Cycle : ' + str(self.DC) + '%\n'
		myStr += 'Nb available subbands : ' + str(self.nbAvailableSubbands) + '\n'
		myStr += 'Symbol Time : ' + str(self.Tsymb) + 'us \n'
		myStr += 'Preamble Length : ' + str(self.preambleLen) + '\n'
		myStr += 'Mac Ovhd Data : ' + str(self.MACOvhdData) + ' bits\n'
		myStr += 'Mac Ovhd Empty Packet : ' + str(self.MACOvhdEmptyPacket) + ' bits\n'
		myStr += 'Time between Tx and Ack : ' + str(self.RECEIVE_DELAY1) + ' us\n'
		return myStr
		
		
if __name__ == '__main__' : 	
	myLoRa = LoRa(0.1,'oldPaper',"EuMin")
	print myLoRa
	
	print "Max Packet time : " + str(myLoRa.maxPacketTime()*pow(10,-3)) + " s"
	print "10 bytes packt time : " + str(myLoRa.dataPacketTime(80)) + " ms"
	print "ackTime : " + str(myLoRa.ackTime()) + " ms"
	
	print "Capacity testing during one day : "
	from pprint import pprint	
	pprint(myLoRa.capacityOutputs(time = 86400000))