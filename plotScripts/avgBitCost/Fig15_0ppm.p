
#expected parameters: 
#_fileName, _initEnergy , _ta, _output

set output "_output"
set style function lines

set key outside below
set key samplen 2 spacing 1 font ",5"
set bmargin 12

set logscale x
unset label	 	# remove any previous labels
set autoscale	# scale axes automatically
set xtic auto	# set xtics automatically
set ytic auto	 # set ytics automatically
TA = _ta / 1000. #s
set title sprintf('$t_a$ =  %1.2f s',TA)
set ylabel "Average Bit Cost in _ltUnit\n"
set xlabel '$s_a$ (bytes)'
set x2label 'Application Throughput $r_a$ (bps)'
set grid
set pointsize 1

plot "_fileName" u ($1*1000):(10*log10($10)) w linespoints lt -1  pt 7 ps 2 lc rgb '#00C0FF' lw 2 t 'LoRa EUMin 0.01 (250 bps)',\
 	 "_fileName" u ($1*1000):(10*log10($11)) w linespoints lt 0  pt 7 ps 2 lc rgb '#00C0FF' lw 2 t 'LoRa EUMin 1 (250 bps)',\
	 "_fileName" u ($1*1000):(10*log10($12)) w linespoints lt -1 pt 6 ps 2 lc rgb '#2677A6' lw 2 t 'LoRa EUMax 0.01 (11 kbps)',\
	 "_fileName" u ($1*1000):(10*log10($13)) w linespoints lt 0 pt 6 ps 2 lc rgb '#2677A6' lw 2 t 'LoRa EUMax 1 (11kpbs)',\
	 \
	 "_fileName" u ($1*1000):(10*log10($14)) w linespoints lt -1 pt 15 ps 2 lc rgb '#0073FF' lw 2 t 'Sigfox 0.01 (100 bps)', \
	 "_fileName" u ($1*1000):(10*log10($15)) w linespoints lt 0  pt 15 ps 2 lc rgb '#0073FF' lw 2 t 'Sigfox 1 (100 bps)', \
	 "_fileName" u ($1*1000):(10*log10($16)) w linespoints lt -1 pt 14 ps 2 lc rgb '#032476' lw 2 t 'Sigfox 0.01 (1000 bps)', \
	 "_fileName" u ($1*1000):(10*log10($17)) w linespoints lt 0  pt 14 ps 2 lc rgb '#032476' lw 2 t 'Sigfox 1 (1000 bps)',\
	 \
	"_fileName" u ($1*1000):(10*log10($3)) w linespoints lt -1 pt 5 ps 2 lc 4 lw 2 t '802.15.4 (0.25)', \
	"_fileName" u ($1*1000):(10*log10($2)) w linespoints lt -1 pt 2 ps 2 lc 7 lw 2 t 'BLE4.2 (1)', \
	"_fileName" u ($1*1000):(10*log10($4)) w linespoints lt 0 pt 4 ps 2 lc rgb '#C6550F' lw 2 t 'TSCH (0.25)' , \
	"_fileName" u ($1*1000):(10*log10($5)) w linespoints lt -1 pt 4 ps 2 lc rgb '#C6550F' lw 2 t 'HW opt.TSCH (0.25)' , \
	 \
	"_fileName" u ($1*1000):(10*log10($6*1000)) w linespoints lt -1 pt 6 ps 2 lc 8 lw 2 t '802.11PSM (11)', \
	 \
	"_fileName" u ($1*1000):(10*log10($7*1000)) w linespoints lt -1 pt 8 ps 2 lc rgb '#14551F' lw 2 t 'ah:MCS10,1Mhz (0.015)' , \
	"_fileName" u ($1*1000):(10*log10($8*1000)) w linespoints lt -1 pt 11 ps 2 lc rgb '#29983C' lw 2 t 'ah:MCS8,2Mhz (7.8)' , \
	"_fileName" u ($1*1000):(10*log10($9*1000)) w linespoints lt -1  pt 12 ps 2 lc rgb '#7EC696' lw 2 t 'ah:MC9,16Mhz (78)', \
	 \
	
