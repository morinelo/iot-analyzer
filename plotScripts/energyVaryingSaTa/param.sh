#!/bin/sh

export PLOTSCRIPT_FOLDER=$1
export MODE=$2
export AUTOGEN_GNUPLOT_SCRIPT=$3
export AUTOGEN_GNUPLOT_SCRIPT_FOLDER=$4
export DAT_FOLDER=$5
export TEX_FOLDER=$6
export RUN_NAME=$7

#Filling header
cat ${PLOTSCRIPT_FOLDER}gnuplotTerminalSetter.p > ${AUTOGEN_GNUPLOT_SCRIPT}

#Filling expected param (file)
#_fileName, _initEnergy , _ta, _title
export TA=$(echo ${AUTOGEN_GNUPLOT_SCRIPT} | sed "s#${AUTOGEN_GNUPLOT_SCRIPT_FOLDER}##g" | sed "s/\.p//g" | sed "s/${MODE}//g" )
export LT_UNIT="mW"
export LOGSCALE_Y="set logscale y"

export OUTPUT=${TEX_FOLDER}${MODE}${TA}.tex
export E0=$(python globConf.py | grep "E0" | sed "s/E0 //g")
export DATFILE=${DAT_FOLDER}${MODE}${TA}.dat
#Replacing parameters from .p file
cat ${PLOTSCRIPT_FOLDER}${MODE}/${RUN_NAME}.p | sed "s#_fileName#${DATFILE}#g" | sed "s#_initEnergy#${E0}#g" \
	| sed "s#_ta#${TA}#g" | sed "s#_output#${OUTPUT}#g" | sed "s#_ltUnit#${LT_UNIT}#g" | sed "s#_logscaleY#${LOGSCALE_Y}#g" >> ${AUTOGEN_GNUPLOT_SCRIPT}