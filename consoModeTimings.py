class consoModeTimings():	
	""" This class aims at simplifying timings handling and printing """ 
		
	def __init__(self, tx, rx, idle, periodLength, name, unit = "N/A") :
		#All parameters are expected in us 
		self.tx = tx
		self.rx = rx 
		self.idle = idle 
		self.sleep = periodLength - (rx + tx + idle)
		self.name = name			
		self.unit = unit
		
	def __repr__(self): 
		myStr = "\n-- " + self.name + " Timings ("+ self.unit + ") --\n"
		myStr += "Tx Time : " + str(self.tx) + "\n"
		myStr += "Rx Time : " + str(self.rx) + "\n"
		myStr += "Idle Time : " + str(self.idle) + "\n"
		myStr += "Sleep Time : " + str(self.sleep) + "\n"
		myStr += "----------------------------- \n"
		return myStr